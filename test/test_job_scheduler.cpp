/**
 * Copyright 2023 Christian Daigneault-St-Arnaud
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
 * following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions
 *    and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the
 *    following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote
 *    products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
// NOLINTBEGIN

#include "catch2/catch.hpp"
#include "job_scheduler.h"
#include "time_util.h"
#include <array>
#include <vector>

using namespace util::job_scheduler;

// Application functions
typedef struct {
    std::uint32_t red;
    std::uint32_t green;
    std::uint32_t blue;
} light_color_t;

// Arguments verification
std::vector<light_color_t> input_args;

void update_light(void* args) {
    if (args == nullptr) {
        input_args.push_back({0, 0, 0});
    } else {
        light_color_t color = *(light_color_t*)args;

        input_args.push_back(color);
    }
}

// Time format taken from class
// timestamp_t bitfield
static constexpr std::uint8_t hour_pos    = 27U;  // Bits 31 ... 27
static constexpr std::uint8_t min_pos     = 21U;  // Bits 26 ... 21
static constexpr std::uint8_t sec_pos     = 15U;  // Bits 20 ... 15
static constexpr std::uint8_t opt_pos     = 14U;  // Bits 14 ... 14
static constexpr std::uint8_t month_pos   = 5U;   // Bits  8 ...  5
static constexpr std::uint8_t day_pos     = 0U;   // Bits  4 ...  0
static constexpr std::uint8_t weekday_pos = 0U;   // Bits  6 ...  0

// options
enum : std::uint32_t { month_day_format = 0U << opt_pos, weekday_format = 1U << opt_pos };

SCENARIO("Job scheduler", "[job scheduler]") {  // NOLINT(readability-function-cognitive-complexity)
    static constexpr std::uint32_t                            schedule_size = 64;
    std::array<Job_schedule::schedule_entry_t, schedule_size> schedule_memory{};

    Job_schedule  schedule{schedule_memory.begin(), schedule_memory.end()};
    Job_scheduler job_scheduler(schedule);

    // Mock
    input_args.clear();

    GIVEN("Initialization") {
        WHEN("constructor is called") {
            THEN("it reserves the necessary memory from the buffer") {
                REQUIRE(job_scheduler.get_schedule().capacity() == schedule_size);
            }
        }
    }

    GIVEN("The scheduler is empty") {

        WHEN("the application insert one job without arguments") {

            Job_scheduler::timestamp_t job1_timestamp = 0x12345678;
            job_scheduler.insert(job1_timestamp, {update_light, nullptr});

            THEN("the time and job are added to schedule") {

                REQUIRE(job_scheduler.get_schedule()[0].first == job1_timestamp);
                REQUIRE(job_scheduler.get_schedule()[0].second.execute == update_light);
                REQUIRE(job_scheduler.get_schedule()[0].second.arguments == nullptr);
            }
        }

        WHEN("the application insert one job with tree arguments") {

            Job_scheduler::timestamp_t job1_timestamp = 0x12345678;
            light_color_t              job1_color     = {.red = 128, .green = 64, .blue = 32};

            job_scheduler.insert(job1_timestamp, {update_light, (void*)&job1_color});

            THEN("the time and job are added to schedule") {

                REQUIRE(job_scheduler.get_schedule()[0].first == job1_timestamp);
                REQUIRE(job_scheduler.get_schedule()[0].second.execute == update_light);
                REQUIRE((*(light_color_t*)(job_scheduler.get_schedule()[0].second.arguments)).red == 128);
                REQUIRE((*(light_color_t*)(job_scheduler.get_schedule()[0].second.arguments)).green == 64);
                REQUIRE((*(light_color_t*)(job_scheduler.get_schedule()[0].second.arguments)).blue == 32);
            }
        }

        WHEN("the application insert a jobs with time format from util::time") {
            util::time::Time_t job_time = {.hour = 13, .min = 25, .sec = 33};
            util::time::Date_t job_date = {.month = 11, .day = 3};

            job_scheduler.insert(job_time, job_date, {update_light, nullptr});

            THEN("the time format is converted to scheduler's compact format") {
                std::uint32_t expected_timestamp = (job_time.hour << hour_pos) | (job_time.min << min_pos) |
                                                   (job_time.sec << sec_pos) | (0U << opt_pos) |
                                                   (job_date.month << month_pos) | (job_date.day << day_pos);

                REQUIRE(job_scheduler.get_schedule()[0].first == expected_timestamp);
                REQUIRE(job_scheduler.get_schedule()[0].second.execute == update_light);
                REQUIRE(job_scheduler.get_schedule()[0].second.arguments == nullptr);
            }
        }

        WHEN("the application insert a jobs with time from util::Time with weekday option") {
            util::time::Time_t job_time = {.hour = 12, .min = 25, .sec = 33};

            job_scheduler.insert(job_time, Job_scheduler::friday | Job_scheduler::saturday, {update_light, nullptr});

            THEN("the time format is converted to scheduler's compact format") {
                std::uint32_t expected_timestamp = (job_time.hour << hour_pos) | (job_time.min << min_pos) |
                                                   (job_time.sec << sec_pos) | (weekday_format) |
                                                   (Job_scheduler::friday) | (Job_scheduler::saturday);

                REQUIRE(job_scheduler.get_schedule()[0].first == expected_timestamp);
                REQUIRE(job_scheduler.get_schedule()[0].second.execute == update_light);
                REQUIRE(job_scheduler.get_schedule()[0].second.arguments == nullptr);
            }
        }

        WHEN("the application insert multiple jobs") {

            util::time::Time_t job1_time = {.hour = 13, .min = 22, .sec = 35};

            util::time::Time_t job2_time = {.hour = 13, .min = 22, .sec = 35};

            util::time::Date_t job2_date = {.month = 11, .day = 3};

            util::time::Time_t job3_time = {.hour = 7, .min = 25, .sec = 33};

            util::time::Time_t job4_time = {.hour = 7, .min = 25, .sec = 33};

            job_scheduler.insert(job1_time, Job_scheduler::friday | Job_scheduler::saturday, {update_light, nullptr});
            job_scheduler.insert(job2_time, job2_date, {update_light, nullptr});
            job_scheduler.insert(job3_time, Job_scheduler::monday, {update_light, nullptr});
            job_scheduler.insert(job4_time, Job_scheduler::weekend, {update_light, nullptr});

            THEN("the jobs are added to schedule in ascending order (time of day, 24h format)") {
                std::uint32_t job1_expected_timestamp = (job1_time.hour << hour_pos) | (job1_time.min << min_pos) |
                                                        (job1_time.sec << sec_pos) | (weekday_format) |
                                                        (Job_scheduler::friday) | (Job_scheduler::saturday);

                std::uint32_t job2_expected_timestamp = (job2_time.hour << hour_pos) | (job2_time.min << min_pos) |
                                                        (job2_time.sec << sec_pos) | (0 << opt_pos) |
                                                        (job2_date.month << month_pos) | (job2_date.day << day_pos);

                std::uint32_t job3_expected_timestamp = (job3_time.hour << hour_pos) | (job3_time.min << min_pos) |
                                                        (job3_time.sec << sec_pos) | (weekday_format) |
                                                        (Job_scheduler::monday);

                std::uint32_t job4_expected_timestamp = (job4_time.hour << hour_pos) | (job4_time.min << min_pos) |
                                                        (job4_time.sec << sec_pos) | (weekday_format) |
                                                        (Job_scheduler::weekend);

                REQUIRE(job_scheduler.get_schedule()[0].first == job3_expected_timestamp);
                REQUIRE(job_scheduler.get_schedule()[1].first == job4_expected_timestamp);
                REQUIRE(job_scheduler.get_schedule()[2].first == job2_expected_timestamp);
                REQUIRE(job_scheduler.get_schedule()[3].first == job1_expected_timestamp);
            }
        }

        WHEN("the application insert multiple jobs with the same timestamp") {

            constexpr Job_scheduler::timestamp_t job1_time   = 0x12345678;
            light_color_t                        job_a_color = {.red = 1, .green = 2, .blue = 3};

            light_color_t job_b_color = {.red = 4, .green = 5, .blue = 6};

            light_color_t job_c_color = {.red = 7, .green = 8, .blue = 9};

            job_scheduler.insert(job1_time, {update_light, (void*)&job_a_color});
            job_scheduler.insert(job1_time, {update_light, (void*)&job_b_color});
            job_scheduler.insert(job1_time, {update_light, (void*)&job_c_color});
            job_scheduler.insert(job1_time, {update_light, nullptr});

            THEN("the first jobs is kept, all new entry with the same timestamp are ignored") {
                // This comportment is chosen to facilitate the management of the arguments.
                // If arguments value are added in an array, overwriting the entry make it complicated
                // for the user to track where the argument was stored.

                REQUIRE(job_scheduler.get_schedule().size() == 1);
                REQUIRE(job_scheduler.get_schedule()[0].first == job1_time);
                REQUIRE(job_scheduler.get_schedule()[0].second.execute == update_light);
                REQUIRE(job_scheduler.get_schedule()[0].second.arguments == (void*)&job_a_color);
            }
        }
    }

    GIVEN("The schedule has a limited size") {
        static constexpr std::uint32_t                                  schedule_size_small = 4;
        std::array<Job_schedule::schedule_entry_t, schedule_size_small> schedule_memory_small{};

        Job_schedule  schedule_small{schedule_memory_small.begin(), schedule_memory_small.end()};
        Job_scheduler job_scheduler_small(schedule_small);

        WHEN("too many event are added to schedule") {

            util::time::Time_t job1_time = {.hour = 00, .min = 01, .sec = 00};

            util::time::Time_t job2_time = {.hour = 02, .min = 10, .sec = 00};

            util::time::Time_t job3_time = {.hour = 03, .min = 10, .sec = 00};

            util::time::Time_t job4_time = {.hour = 04, .min = 10, .sec = 00};

            util::time::Time_t job5_time = {.hour = 05, .min = 10, .sec = 00};

            util::time::Time_t job6_time = {.hour = 06, .min = 10, .sec = 00};

            bool success[6];
            success[0] = job_scheduler_small.insert(job1_time, Job_scheduler::everyday, {update_light, nullptr});
            success[1] = job_scheduler_small.insert(job2_time, Job_scheduler::everyday, {update_light, nullptr});
            success[2] = job_scheduler_small.insert(job3_time, Job_scheduler::everyday, {update_light, nullptr});
            success[3] = job_scheduler_small.insert(job4_time, Job_scheduler::everyday, {update_light, nullptr});
            success[4] = job_scheduler_small.insert(job5_time, Job_scheduler::everyday, {update_light, nullptr});
            success[5] = job_scheduler_small.insert(job6_time, Job_scheduler::everyday, {update_light, nullptr});

            THEN("events 5 & 6 are not added") {
                REQUIRE(job_scheduler_small.get_schedule().size() == schedule_size_small);
                REQUIRE(success[0] == true);
                REQUIRE(success[1] == true);
                REQUIRE(success[2] == true);
                REQUIRE(success[3] == true);
                REQUIRE(success[4] == false);
                REQUIRE(success[5] == false);
            }
        }
    }

    GIVEN("The schedule has multiple jobs") {

        Job_scheduler::timestamp_t job1_timestamp = 0x12345678;
        Job_scheduler::timestamp_t job2_timestamp = 0x22345678;
        Job_scheduler::timestamp_t job3_timestamp = 0x32345678;
        Job_scheduler::timestamp_t job4_timestamp = 0x42345678;

        job_scheduler.insert(job1_timestamp, {update_light, nullptr});
        job_scheduler.insert(job2_timestamp, {update_light, nullptr});
        job_scheduler.insert(job3_timestamp, {update_light, nullptr});

        WHEN("application remove a job") {

            job_scheduler.erase(job2_timestamp);

            THEN("the job is removed") {
                auto it = job_scheduler.get_schedule().begin();

                auto it_job1 = it;
                auto it_job3 = ++it;

                REQUIRE(job_scheduler.get_schedule().size() == 2);
                REQUIRE(it_job1->first == job1_timestamp);
                REQUIRE(it_job3->first == job3_timestamp);
            }
        }

        WHEN("application remove a job that doesn't exist") {

            job_scheduler.erase(job4_timestamp);

            THEN("nothing happened") {
                auto it = job_scheduler.get_schedule().begin();

                auto it_job1 = it;
                auto it_job2 = ++it;
                auto it_job3 = ++it;

                REQUIRE(job_scheduler.get_schedule().size() == 3);
                REQUIRE(it_job1->first == job1_timestamp);
                REQUIRE(it_job2->first == job2_timestamp);
                REQUIRE(it_job3->first == job3_timestamp);
            }
        }
    }

    GIVEN("The application added multiple jobs to scheduler") {

        util::time::Time_t job1_time = {.hour = 7, .min = 30, .sec = 00};

        util::time::Time_t job2_time = {.hour = 9, .min = 30, .sec = 00};

        util::time::Time_t job3_time = {.hour = 9, .min = 30, .sec = 00};

        util::time::Date_t job3_date = {
            .month = 6,
            .day   = 12,
        };

        util::time::Time_t job4_5_time = {.hour = 11, .min = 11, .sec = 11};

        light_color_t job1_args = {1, 1, 1};
        light_color_t job2_args = {2, 2, 2};
        light_color_t job3_args = {3, 3, 3};
        light_color_t job4_args = {4, 4, 4};
        light_color_t job5_args = {5, 5, 5};

        job_scheduler.insert(job1_time, Job_scheduler::everyday, {update_light, (void*)&job1_args});
        job_scheduler.insert(job2_time, Job_scheduler::monday, {update_light, (void*)&job2_args});
        job_scheduler.insert(job2_time, Job_scheduler::tuesday, {update_light, (void*)&job2_args});
        job_scheduler.insert(job3_time, job3_date, {update_light, (void*)&job3_args});
        job_scheduler.insert(job4_5_time, Job_scheduler::everyday, {update_light, (void*)&job4_args});
        job_scheduler.insert(job4_5_time, Job_scheduler::monday, {update_light, (void*)&job5_args});

        WHEN("there is no job left to execute") {

            util::time::Calendar_t current_time{
                .date = {.month = 11, .day = 16, .week_day = util::time::Week_day::monday},

                .time = { .hour = 22, .min = 30,                                 .sec = 0}
            };

            job_scheduler.execute(current_time);  // Move search cursor to 22h30

            // Application call
            job_scheduler.execute(current_time);

            THEN("nothing") {
                // Used to step in search algorithm
                REQUIRE(input_args.empty());
            }
        }

        WHEN("it is time to execute the job scheduled everyday at 7h30 ") {

            util::time::Calendar_t current_time{
                .date = {.month = 11, .day = 16, .week_day = util::time::Week_day::monday},

                .time = {  .hour = 7, .min = 30,                                 .sec = 0}
            };

            job_scheduler.execute(current_time);

            THEN("the job is executed") {
                REQUIRE(input_args.size() == 1);
                REQUIRE(input_args[0].red == 1);
            }
        }

        WHEN("it is time to execute the job scheduled monday at 9h30 ") {

            util::time::Calendar_t current_time{
                .date = {.month = 11, .day = 16, .week_day = util::time::Week_day::monday},

                .time = {  .hour = 9, .min = 25,                                 .sec = 0}
            };

            job_scheduler.execute(current_time);  // Move search cursor to 9h25

            // Application
            current_time.time.hour = 9;
            current_time.time.min  = 30;
            job_scheduler.execute(current_time);

            THEN("the job is executed") {
                // Used to step in search algorithm
                REQUIRE(input_args.size() == 1);
                REQUIRE(input_args[0].red == 2);
            }
        }

        WHEN("it is time to execute the job scheduled June 12th at 9h30 ") {

            util::time::Calendar_t current_time{
                .date = {.month = 6, .day = 12, .week_day = util::time::Week_day::friday},

                .time = { .hour = 9, .min = 25,                                 .sec = 0}
            };

            job_scheduler.execute(current_time);  // Move search cursor to 9h25

            // Application
            current_time.time.hour = 9;
            current_time.time.min  = 30;
            job_scheduler.execute(current_time);

            THEN("the job is executed") {
                REQUIRE(input_args.size() == 1);
                REQUIRE(input_args[0].red == 3);
            }
        }

        WHEN("two jobs are schedule at the same time") {

            util::time::Calendar_t current_time{
                .date = {.month = 6, .day = 12, .week_day = util::time::Week_day::monday},

                .time = {.hour = 11, .min = 11,                                .sec = 11}
            };

            job_scheduler.execute(current_time);

            THEN("the job with the time format giving the lowest value is executed first") {
                REQUIRE(input_args.size() == 2);
                REQUIRE(input_args[0].red == 5);
                REQUIRE(input_args[1].red == 4);
            }
        }
    }

    GIVEN("Application is running") {

        util::time::Calendar_t current_time{
            .date = {.month = 11, .day = 16, .week_day = util::time::Week_day::monday},

            .time = { .hour = 10, .min = 00,                                 .sec = 0}
        };

        util::time::Time_t job1_time = {.hour = 7, .min = 30, .sec = 00};

        util::time::Time_t job2_time = {.hour = 9, .min = 30, .sec = 00};

        util::time::Time_t job3_time = {.hour = 12, .min = 10, .sec = 00};

        light_color_t job1_args = {1, 1, 1};
        light_color_t job2_args = {2, 2, 2};
        light_color_t job3_args = {3, 3, 3};

        job_scheduler.insert(job1_time, Job_scheduler::everyday, {update_light, (void*)&job1_args});
        job_scheduler.insert(job2_time, Job_scheduler::everyday, {update_light, (void*)&job2_args});

        job_scheduler.execute(current_time);  // Set search cursor to 10h00

        WHEN("a job is inserted and must execute shortly") {

            job_scheduler.insert(job3_time, Job_scheduler::monday, {update_light, (void*)&job3_args});

            current_time.time.hour = 12;
            current_time.time.min  = 10;

            job_scheduler.execute(current_time);

            THEN("the new job is executed") {
                REQUIRE(input_args.size() == 1);
                REQUIRE(input_args[0].red == 3);
            }
        }

        WHEN("a job is inserted and must execute the next day") {

            job_scheduler.insert(job1_time, Job_scheduler::monday, {update_light, (void*)&job3_args});

            current_time.time.hour = 12;
            current_time.time.min  = 10;

            job_scheduler.execute(current_time);

            THEN("nothing") {
                REQUIRE(input_args.empty());
            }
        }

        WHEN("application check call execute within the same second ") {

            job_scheduler.insert(job3_time, Job_scheduler::monday, {update_light, (void*)&job3_args});

            current_time.time.hour = 12;
            current_time.time.min  = 10;
            current_time.time.sec  = 0;

            job_scheduler.execute(current_time);
            job_scheduler.execute(current_time);
            job_scheduler.execute(current_time);

            THEN("job is executed only once") {
                REQUIRE(input_args.size() == 1);
                REQUIRE(input_args[0].red == 3);
            }
        }

        WHEN("a new day begin") {
            current_time.time.hour     = 7;
            current_time.time.min      = 30;
            current_time.date.day      = 17;
            current_time.date.week_day = util::time::Week_day::tuesday;

            job_scheduler.execute(current_time);

            THEN("the job is executed the next day") {
                REQUIRE(input_args.size() == 1);
                REQUIRE(input_args[0].red == 1);
            }
        }
    }

    GIVEN("The application added multiple jobs to scheduler, it is 13h25") {

        util::time::Calendar_t current_time{
            .date = {.month = 11, .day = 16, .week_day = util::time::Week_day::monday},

            .time = { .hour = 13, .min = 25,                                 .sec = 0}
        };

        util::time::Time_t job1_time = {.hour = 13, .min = 29, .sec = 00};

        util::time::Time_t job2_time = {.hour = 13, .min = 30, .sec = 00};

        light_color_t job1_args = {1, 1, 1};
        light_color_t job2_args = {2, 2, 2};

        job_scheduler.insert(job1_time, Job_scheduler::everyday, {update_light, (void*)&job1_args});
        job_scheduler.insert(job2_time, Job_scheduler::everyday, {update_light, (void*)&job2_args});

        job_scheduler.execute(current_time);  // Set search cursor to 13h25

        REQUIRE(input_args.empty());

        WHEN("we allow to be 5 min late, the time reach 13:30:29") {

            current_time.time.hour = 13;
            current_time.time.min  = 30;
            current_time.time.sec  = 29;

            job_scheduler.allow_to_be_late_by({.hour = 0, .min = 5, .sec = 0, .ms = 0});
            job_scheduler.execute(current_time);

            THEN("the jobs scheduled at 13h29 and 13h30 are executed") {
                REQUIRE(input_args.size() == 2);
                REQUIRE(input_args[0].red == 1);
                REQUIRE(input_args[1].red == 2);
            }
        }

        WHEN("we allow to be 30 sec late, the time reach 13:30:29") {
            current_time.time.hour = 13;
            current_time.time.min  = 30;
            current_time.time.sec  = 29;

            job_scheduler.allow_to_be_late_by({.hour = 0, .min = 0, .sec = 30, .ms = 0});
            job_scheduler.execute(current_time);

            THEN("the jobs scheduled at 13h30 is executed") {
                REQUIRE(input_args.size() == 1);
                REQUIRE(input_args[0].red == 2);
            }
        }
    }

    GIVEN("Application has two new schedules") {
        static constexpr std::uint32_t                                new_schedule_size = 32;
        std::array<Job_schedule::schedule_entry_t, new_schedule_size> new_schedule_memory{};
        Job_schedule new_schedule{new_schedule_memory.begin(), new_schedule_memory.end()};

        WHEN("the application switch to schedule 2") {

            job_scheduler.set_schedule(new_schedule);

            THEN("job scheduler follow the new schedule and reset the search index") {
                REQUIRE(job_scheduler.get_schedule().capacity() == new_schedule_size);
            }
        }
    }

    GIVEN("Timestamp to util::Time::time_t conversion") {
        WHEN("timestamp is converted to util::time::time_t") {
            constexpr std::size_t expected_hour = 13;
            constexpr std::size_t expected_min  = 46;
            constexpr std::size_t expected_sec  = 24;
            constexpr std::size_t expected_ms   = 0;

            constexpr Job_scheduler::timestamp_t timestamp =
                expected_hour << hour_pos | expected_min << min_pos | expected_sec << sec_pos;

            util::time::Time_t t = Job_scheduler::timestamp_to_time(timestamp);

            THEN("time_t correspond to given time") {
                REQUIRE(t.hour == expected_hour);
                REQUIRE(t.min == expected_min);
                REQUIRE(t.sec == expected_sec);
                REQUIRE(t.ms == expected_ms);
            }
        }
    }
}
// NOLINTEND
