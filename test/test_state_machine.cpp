/**
 * Copyright 2023 Christian Daigneault-St-Arnaud
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
 * following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions
 *    and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the
 *    following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote
 *    products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
// NOLINTBEGIN

#include "catch2/catch.hpp"
#include "motor_state_machine.h"
#include <vector>
using namespace util::sm;

/*
 *  State machine is tested has if we are controlling a motor.
 *  Abbreviation :
 *  fsm : Finite State Machine
 */

class Drive_mock : public Drive {
public:
    void test(std::string message) {
        actions.push_back(message);
    }

    std::vector<std::string> actions;
};

TEST_CASE("Motor state Machine", "[fsm]") {

    Drive_mock drive;

    Motor_fsm    fsm(drive);
    state_name_t current_state_name;

    SECTION("Initialization") {
        current_state_name = fsm.get_current_state_name();
        REQUIRE(Motor_fsm::State_name::state_idle == current_state_name);
    }

    SECTION("Set motor speed, State Idle to Running") {
        fsm.change_speed(50);

        current_state_name = fsm.get_current_state_name();

        REQUIRE(drive.actions.at(0) == "Execute running 50");
        REQUIRE(Motor_fsm::State_name::state_running == current_state_name);
    }

    SECTION("Set motor speed while running") {
        fsm.change_speed(50);
        fsm.change_speed(100);

        current_state_name = fsm.get_current_state_name();

        REQUIRE(drive.actions.at(0) == "Execute running 50");
        REQUIRE(drive.actions.at(1) == "Execute running 100");
        REQUIRE(Motor_fsm::State_name::state_running == current_state_name);
    }

    SECTION("Halt from running") {
        fsm.change_speed(50);
        fsm.halt();

        current_state_name = fsm.get_current_state_name();

        REQUIRE(drive.actions.at(0) == "Execute running 50");
        REQUIRE(drive.actions.at(1) == "Execute stop");
        REQUIRE(drive.actions.at(2) == "Execute idle");
        REQUIRE(Motor_fsm::State_name::state_idle == current_state_name);
    }

    SECTION("Throw event not handled by any state (force error)") {
        fsm.change_speed(50);
        fsm.trig_error();

        current_state_name = fsm.get_current_state_name();

        REQUIRE(drive.actions.at(0) == "Execute running 50");
        REQUIRE(drive.actions.at(1) == "Execute error");
        REQUIRE(Motor_fsm::State_name::state_error == current_state_name);
    }

    SECTION("Execute state not in matrix (Error state)") {
        fsm.trig_error();
        fsm.trig_error();

        current_state_name = fsm.get_current_state_name();

        REQUIRE(Motor_fsm::State_name::state_error == current_state_name);
    }
}
// NOLINTEND
