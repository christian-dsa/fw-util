/**
 * Copyright 2023 Christian Daigneault-St-Arnaud
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
 * following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions
 *    and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the
 *    following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote
 *    products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
// NOLINTBEGIN

#include "catch2/catch.hpp"
#include "littlefs.h"
#include <string>

using namespace util;

class Ram_io : public littlefs::Block_device {
public:
    Ram_io() = default;

    void init() {
        std::fill(&ram_memory[0][0], &ram_memory[nb_page - 1][page_size - 1], 0xFFFFFFFF);
    }

    int read(const struct lfs_config* c, lfs_block_t block, lfs_off_t off, void* buffer, lfs_size_t size) override {
        auto write_iterator = (std::uint8_t*)buffer;

        for (std::uint32_t i = 0; i < size; ++i) {
            *write_iterator = ram_memory[block][off + i];
            ++write_iterator;
        }

        return 0;
    }

    int
    prog(const struct lfs_config* c, lfs_block_t block, lfs_off_t off, const void* buffer, lfs_size_t size) override {
        auto write_iterator = (const std::uint8_t*)buffer;

        for (std::uint32_t i = 0; i < size; ++i) {
            ram_memory[block][off + i] = *write_iterator;
            ++write_iterator;
        }

        return 0;
    }

    int erase(const struct lfs_config* c, lfs_block_t block) override {
        std::fill(&ram_memory[block][0], &ram_memory[block][page_size - 1], 0xFFFFFFFF);

        return 0;
    }

    int sync(const struct lfs_config* c) override {
        return 0;
    }

    // 32 bits system
    static constexpr std::uint32_t nb_page   = 8;
    static constexpr std::uint32_t page_size = 2048;
    uint32_t                       ram_memory[nb_page][page_size]{};
};

static Ram_io io;

SCENARIO("Littlefs is not mounted", "[littlefs]") {

    GIVEN("File system is stored in RAM, Write & Read size: 1 byte ") {
        constexpr std::uint32_t                                         cache_size     = 64;
        constexpr std::uint32_t                                         lookahead_size = 64;
        std::array<std::uint8_t, cache_size>                            fs_prog{};
        std::array<std::uint8_t, cache_size>                            fs_read{};
        alignas(std::uint32_t) std::array<std::uint8_t, lookahead_size> fs_lookahead{};

        io.init();
        littlefs::Error       error;
        littlefs::Fs_config   fs_config = {.read_size        = 1,
                                           .prog_size        = 1,
                                           .block_size       = Ram_io::page_size,
                                           .block_count      = Ram_io::nb_page,
                                           .block_cycles     = 500,
                                           .name_max         = 0,
                                           .file_max         = 0,
                                           .attr_max         = 0,
                                           .metadata_max     = 0,
                                           .read_buffer      = fs_read.data(),
                                           .prog_buffer      = fs_prog.data(),
                                           .cache_size       = cache_size,
                                           .lookahead_buffer = fs_lookahead.data(),
                                           .lookahead_size   = lookahead_size};
        littlefs::File_system fs;
        fs.init(io, fs_config);

        WHEN("file system is mounted the first time (never formatted)") {
            fs.mount(error);

            THEN("system is in error") {
                REQUIRE(error == littlefs::Error::corrupt);
            }
        }

        WHEN("file system is mounted the first time and formatted") {
            fs.format(error);
            fs.mount(error);

            THEN("system is in error") {
                REQUIRE(error == littlefs::Error::ok);
                REQUIRE(fs.is_mount());
            }
        }
        fs.unmount();
    }

    GIVEN("File system is stored in RAM, wrong config") {

        constexpr std::uint32_t                                         cache_size     = 64;
        constexpr std::uint32_t                                         lookahead_size = 64;
        std::array<std::uint8_t, cache_size>                            fs_prog{};
        std::array<std::uint8_t, cache_size>                            fs_read{};
        alignas(std::uint32_t) std::array<std::uint8_t, lookahead_size> fs_lookahead{};

        io.init();
        littlefs::Error     error;
        littlefs::Fs_config fs_config = {.read_size        = 16,
                                         .prog_size        = 16,
                                         .block_size       = Ram_io::page_size,
                                         .block_count      = Ram_io::nb_page,
                                         .block_cycles     = 500,
                                         .name_max         = 0,
                                         .file_max         = 0,
                                         .attr_max         = 0,
                                         .metadata_max     = 0,
                                         .read_buffer      = fs_read.data(),
                                         .prog_buffer      = fs_prog.data(),
                                         .cache_size       = cache_size,
                                         .lookahead_buffer = fs_lookahead.data(),
                                         .lookahead_size   = lookahead_size};

        WHEN("block_size is not a multiple of read and program sizes") {
            fs_config.read_size = 48;  // block size = 2048. 2048 / 48 = 42.66666
            fs_config.prog_size = 48;

            littlefs::File_system fs;
            fs.init(io, fs_config, error);

            THEN("ctor return error::invalid") {
                REQUIRE(error == littlefs::Error::invalid);
            }
        }

        WHEN("cache_size is not a multiple of read and program sizes") {
            fs_config.cache_size = 24;

            littlefs::File_system fs;
            fs.init(io, fs_config, error);

            THEN("ctor return error::invalid") {
                REQUIRE(error == littlefs::Error::invalid);
            }
        }

        WHEN("cache_size is not a factor of the block size") {
            fs_config.cache_size = 48;
            littlefs::File_system fs;
            fs.init(io, fs_config, error);

            THEN("ctor return error::invalid") {
                REQUIRE(error == littlefs::Error::invalid);
            }
        }

        WHEN("lookahead_size is not a multiple of 8") {
            fs_config.lookahead_size = 66;
            littlefs::File_system fs;
            fs.init(io, fs_config, error);

            THEN("ctor return error::invalid") {
                REQUIRE(error == littlefs::Error::invalid);
            }
        }

        WHEN("lookahead_buffer is not aligned to a 32-bit boundary") {
            // Force alignment to 8 bit
            fs_config.lookahead_buffer = fs_lookahead.data() + 1;
            littlefs::File_system fs;
            fs.init(io, fs_config, error);

            THEN("ctor return error::invalid") {
                REQUIRE(error == littlefs::Error::invalid);
            }
        }
    }
}

SCENARIO("Littlefs is mounted", "[littlefs]") {  // NOLINT
    // File system
    constexpr std::uint32_t                                         cache_size     = 16;
    constexpr std::uint32_t                                         lookahead_size = 16;
    std::array<std::uint8_t, cache_size>                            fs_prog{};
    std::array<std::uint8_t, cache_size>                            fs_read{};
    alignas(std::uint32_t) std::array<std::uint8_t, lookahead_size> fs_lookahead{};

    io.init();
    littlefs::Error error;

    littlefs::Fs_config fs_config = {.read_size        = 8,
                                     .prog_size        = 8,
                                     .block_size       = Ram_io::page_size,
                                     .block_count      = Ram_io::nb_page,
                                     .block_cycles     = 500,
                                     .name_max         = 0,
                                     .file_max         = 0,
                                     .attr_max         = 0,
                                     .metadata_max     = 0,
                                     .read_buffer      = fs_read.data(),
                                     .prog_buffer      = fs_prog.data(),
                                     .cache_size       = cache_size,
                                     .lookahead_buffer = fs_lookahead.data(),
                                     .lookahead_size   = lookahead_size};

    littlefs::File_system fs;
    fs.init(io, fs_config);

    fs.format(error);
    fs.mount(error);

    GIVEN("file object has no cache") {
        littlefs::File app_file(fs, nullptr);

        WHEN("file is opened") {
            app_file.open("foo.txt", "w", error);

            THEN("there is a no_memory error") {
                REQUIRE(error == littlefs::Error::no_memory);
            }
        }
    }

    GIVEN("file exit but is not open") {
        // Create file
        std::array<std::uint8_t, cache_size> file_cache{};
        littlefs::File                       app_file(fs, file_cache.data());

        app_file.open("bar.txt", "w", error);
        app_file.write("This is bar file!", error);
        std::int32_t size = app_file.size(error);
        app_file.close(error);

        REQUIRE(error == littlefs::Error::ok);
        REQUIRE(size == sizeof("This is bar file!"));

        WHEN("seek is called") {
            app_file.seek(0, littlefs::File::Seek::end, error);
            THEN("the operation is invalid") {
                REQUIRE(error == littlefs::Error::invalid);
            }
        }

        WHEN("sync is called") {
            app_file.sync(error);
            THEN("the operation is invalid") {
                REQUIRE(error == littlefs::Error::invalid);
            }
        }

        WHEN("truncate is called") {
            app_file.truncate(0, error);
            THEN("the operation is invalid") {
                REQUIRE(error == littlefs::Error::invalid);
            }
        }

        WHEN("tell is called") {
            app_file.tell(error);
            THEN("the operation is invalid") {
                REQUIRE(error == littlefs::Error::invalid);
            }
        }

        WHEN("rewind is called") {
            app_file.rewind(error);
            THEN("the operation is invalid") {
                REQUIRE(error == littlefs::Error::invalid);
            }
        }
    }

    GIVEN("file 'foo.txt' doesn't exists") {
        // File
        std::array<std::uint8_t, cache_size> file_cache{};
        littlefs::File                       app_file(fs, file_cache.data());

        WHEN("file is closed") {
            app_file.close(error);

            THEN("there is an error") {
                REQUIRE(error == littlefs::Error::invalid);
            }
        }

        WHEN("file is opened read only") {
            app_file.open("foo.txt", "r", error);

            THEN("there is an error since the file doesn't exist") {
                REQUIRE(error == littlefs::Error::no_entry);
            }
        }

        WHEN("file is opened write only") {
            app_file.open("foo.txt", "w", error);

            THEN("file is created") {
                // File created
                REQUIRE(error == littlefs::Error::ok);
                REQUIRE(app_file.size(error) == 0);
                REQUIRE(error == littlefs::Error::ok);
            }
        }

        WHEN("file is opened write append") {
            app_file.open("foo.txt", "a", error);

            THEN("file is created") {
                // File created
                REQUIRE(error == littlefs::Error::ok);
                REQUIRE(app_file.size(error) == 0);
                REQUIRE(error == littlefs::Error::ok);
            }
        }

        WHEN("user get size while no file is open") {
            std::int32_t size = app_file.size(error);

            THEN("the operation is invalid") {
                REQUIRE(error == littlefs::Error::invalid);
                REQUIRE(size == -1);
            }
        }
    }

    GIVEN("file 'foo.txt' exist and it is opened write only") {
        // File
        std::array<std::uint8_t, cache_size> file_cache{};
        littlefs::File                       app_file(fs, file_cache.data());
        app_file.open("foo.txt", "w", error);
        REQUIRE(error == littlefs::Error::ok);

        WHEN("user write to file") {
            app_file.write("hello foo!", error);
            app_file.close(error);

            THEN("write is successful") {
                app_file.open("foo.txt", "r", error);
                std::int32_t size = app_file.size(error);

                REQUIRE(error == littlefs::Error::ok);
                REQUIRE(size == sizeof("hello foo!"));
            }
        }

        WHEN("user try to read") {
            std::array<char, 8> buffer{0};
            app_file.read(buffer.data(), 1, error);

            THEN("there is an error and file is not read") {
                REQUIRE(error == littlefs::Error::invalid);
                REQUIRE(buffer[0] == 0);
            }
        }

        WHEN("file is closed then write (but it is not opened)") {
            app_file.close(error);
            std::byte data{};

            std::int32_t size = app_file.write(&data, 1, error);

            THEN("operation is invalid") {
                REQUIRE(error == littlefs::Error::invalid);
                REQUIRE(size == 0);
            }
        }
    }

    GIVEN("file 'foo.txt' exist and it is opened read only") {
        // Create file
        std::array<std::uint8_t, cache_size> file_cache{};
        littlefs::File                       app_file(fs, file_cache.data());

        app_file.open("foo.txt", "w", error);
        app_file.write("I am foo!", error);
        app_file.close(error);

        app_file.open("foo.txt", "r", error);
        REQUIRE(error == littlefs::Error::ok);

        WHEN("the file is opened again") {
            app_file.open("foo.txt", "r", error);

            THEN("the new open is ignored") {
                REQUIRE(error == littlefs::Error::invalid);
            }
        }

        WHEN("user try to write") {
            std::int32_t w_size = app_file.write("hello foo!", error);

            THEN("there is an error and file is still the same") {
                REQUIRE(error == littlefs::Error::invalid);
                REQUIRE(w_size == 0);
            }
        }

        WHEN("user read") {
            std::array<char, 16> r_buffer{};
            std::int32_t         f_size = app_file.size(error);
            std::int32_t         r_size = app_file.read(r_buffer.data(), f_size, error);

            THEN("there is no error and file is still the same") {
                REQUIRE(error == littlefs::Error::ok);
                REQUIRE(r_size == f_size);
                REQUIRE(f_size == sizeof("I am foo!"));

                std::string str_buffer(r_buffer.data(),
                                       (r_size - 1));  // -1 since the NULL at the end is there by default
                REQUIRE(str_buffer == "I am foo!");
            }
        }

        WHEN("file is closed then read (but it is not opened)") {
            app_file.close(error);
            std::byte data{};

            std::int32_t size = app_file.read(&data, 1, error);

            THEN("operation is invalid") {
                REQUIRE(error == littlefs::Error::invalid);
                REQUIRE(size == 0);
            }
        }
    }

    GIVEN("file 'foo.txt' exist and it is opened append write only") {
        // Create file
        std::array<std::uint8_t, cache_size> file_cache{};
        littlefs::File                       app_file(fs, file_cache.data());

        WHEN("user try to read") {
            struct s_foo_t {
                int a;
                int b;
            };
            s_foo_t s_foo{};
            app_file.read(s_foo, error);

            THEN("there is an error and file is not read") {
                REQUIRE(error == littlefs::Error::invalid);
            }
        }
    }

    GIVEN("file 'bar.txt' exist and already has text in it") {
        // Create file
        std::array<std::uint8_t, cache_size> file_cache{};
        littlefs::File                       app_file(fs, file_cache.data());

        app_file.open("bar.txt", "w", error);
        app_file.write("This is bar file!", error);
        std::int32_t size = app_file.size(error);
        app_file.close(error);

        REQUIRE(error == littlefs::Error::ok);
        REQUIRE(size == sizeof("This is bar file!"));

        WHEN("file is opened write only") {
            app_file.open("bar.txt", "w", error);

            THEN("a new file is created") {
                REQUIRE(app_file.size(error) == 0);
                REQUIRE(error == littlefs::Error::ok);
            }
        }

        WHEN("file is opened read & write") {
            app_file.open("bar.txt", "r+", error);

            THEN("the current file doesn't change") {
                REQUIRE(app_file.size(error) == sizeof("This is bar file!"));
            }
            THEN("we can write in file") {
                size = app_file.write("hello", error);  // Overwrite the first 6 characters
                REQUIRE(error == littlefs::Error::ok);
                REQUIRE(size == sizeof("hello"));
            }
        }

        WHEN("file is opened write & read") {
            app_file.open("bar.txt", "w+", error);

            THEN("a new file is created") {
                REQUIRE(app_file.size(error) == 0);
            }
            THEN("we can read file") {
                std::array<char, 1> buffer{};
                app_file.read(buffer.data(), buffer.size(), error);
                REQUIRE(error == littlefs::Error::ok);
            }
        }

        WHEN("file is opened write append") {
            app_file.open("bar.txt", "a", error);

            THEN("the current file doesn't change") {
                REQUIRE(app_file.size(error) == sizeof("This is bar file!"));
            }
        }
    }

    GIVEN("file 'bar.txt' exist and already has text in it, opened 'a+'") {
        // Create file
        std::array<std::uint8_t, cache_size> file_cache{};
        littlefs::File                       app_file(fs, file_cache.data());

        app_file.open("bar.txt", "w", error);
        app_file.write("This is bar file!", error);
        std::int32_t size = app_file.size(error);
        app_file.close(error);

        REQUIRE(error == littlefs::Error::ok);
        REQUIRE(size == sizeof("This is bar file!"));

        app_file.open("bar.txt", "a+", error);

        WHEN("new data is written to file") {
            app_file.write("New line!", error);
            app_file.sync(error);

            THEN("size increase") {
                size = app_file.size(error);
                REQUIRE(size == sizeof("This is bar file!\0New line!"));
                REQUIRE(app_file.tell(error) == size);
            }
            THEN("data is written at the end of file and can be read") {
                char file_buffer[64];
                size = app_file.size(error);
                app_file.rewind(error);
                app_file.read(file_buffer, size, error);
                REQUIRE(error == littlefs::Error::ok);

                std::string  file(file_buffer, size - 1);
                std::uint8_t i = file.find('\0');  // Replace NULL character between the 2 writes. Makes assert easier
                file[i]        = ' ';
                REQUIRE(file == "This is bar file! New line!");
            }
        }
    }

    GIVEN("file is written and read in loop") {
        // File
        std::array<std::uint8_t, cache_size> file_cache{};
        littlefs::File                       app_file(fs, file_cache.data());

        for (std::size_t i = 1; i < 8; ++i) {
            std::size_t j;

            app_file.open("foo.txt", "w", error);
            app_file.write(i);
            app_file.close();

            app_file.open("foo.txt", "r", error);
            app_file.read(j);
            app_file.close();

            REQUIRE(i == j);
        }
    }
    /// Experimentation
    GIVEN("File is created & opened, File system is using user's buffer") {
        // File
        std::array<char, 64> file_cache{};
        littlefs::File       app_file(fs, file_cache.data());
        REQUIRE(error == littlefs::Error::ok);

        WHEN("an object with 3 variables is written in file and read") {
            typedef struct {
                std::uint32_t value_a;
                std::uint32_t value_b;
                std::uint32_t value_c;
            } data_t;

            data_t data_wr = {.value_a = 0x01, .value_b = 0x23, .value_c = 0x45};

            app_file.open("/text.txt", "w+", error);
            app_file.write(data_wr, error);
            app_file.close(error);  // IMPORTANT! File is not writen until file is closed successfully

            data_t data_rd;
            app_file.open("/text.txt", "r", error);
            std::int32_t read_size = app_file.read(data_rd, error);
            app_file.close(error);

            THEN("the data read is exactly the same as the original") {
                REQUIRE(data_wr.value_a == data_rd.value_a);
                REQUIRE(data_wr.value_b == data_rd.value_b);
                REQUIRE(data_wr.value_c == data_rd.value_c);
                REQUIRE(read_size == 12);
                REQUIRE(error == littlefs::Error::ok);
            }
        }

        WHEN("a variable string is writen in a file") {
            std::string str_wr("application string");
            std::string str_rd;

            app_file.open("/text.txt", "w+", error);
            app_file.write(str_wr.c_str(), str_wr.size(), error);
            app_file.close(error);  // IMPORTANT! File is not writen until file is closed successfully

            std::uint32_t file_size;
            std::int32_t  read_size;
            char          file_read[255];

            app_file.open("/text.txt", "a+", error);
            file_size = app_file.size(error);
            read_size = app_file.read(file_read, file_size, error);
            str_rd.append(file_read, file_size);
            app_file.close(error);

            THEN("both string are equals") {
                REQUIRE(str_wr == str_rd);
                REQUIRE(read_size == file_size);
            }
        }
    }

    /// Directory
    GIVEN("File system is brand new") {

        WHEN("a directory is created") {
            fs.mkdir("/dir", error);

            THEN("there is no error") {
                REQUIRE(error == littlefs::Error::ok);
            }
            THEN("statistic says it is a directory") {
                littlefs::info info;
                fs.statistics("/dir", info, error);
                REQUIRE(error == littlefs::Error::ok);
                REQUIRE(info.type == LFS_TYPE_DIR);
            }
        }

        WHEN("directory is opened but doesn't exist") {
            fs.open("/dir", error);

            THEN("error no entry") {
                REQUIRE(error == littlefs::Error::no_entry);
            }
        }

        WHEN("directory is closed but doesn't exist") {
            fs.close(error);

            THEN("operation is invalid") {
                REQUIRE(error == littlefs::Error::invalid);
            }
        }
    }

    GIVEN("File system has a directory") {
        fs.mkdir("/dir", error);  // File system here really has 2 directories. / and dir
        REQUIRE(error == littlefs::Error::ok);

        WHEN("directory is opened") {
            fs.open("/dir", error);

            THEN("there is no error") {
                REQUIRE(error == littlefs::Error::ok);
            }
        }

        WHEN("directory is made again") {
            fs.mkdir("/dir", error);

            THEN("error, directory already exist") {
                REQUIRE(error == littlefs::Error::exist);
            }
        }

        WHEN("a new directory is created") {
            fs.mkdir("/foo", error);  // A directory need 2 blocks

            THEN("there is no error") {
                REQUIRE(error == littlefs::Error::ok);
            }
        }

        WHEN("directory is read") {
            littlefs::info info;
            fs.open("/dir", error);
            fs.read(info, error);

            THEN("we see directory is empty") {
                REQUIRE(info.type == LFS_TYPE_DIR);
                REQUIRE(error == littlefs::Error::ok);
            }
        }
    }

    // Not a test, experimenting with directory
    GIVEN("File system has multiples directory with files in it") {
        // File
        std::array<char, 64> file_cache{};
        littlefs::File       app_file(fs, file_cache.data());

        // Directory
        fs.mkdir("/dir_bar", error);
        fs.mkdir("/dir_foo", error);
        fs.mkdir("/dir_foo/dir_deep", error);

        // Files
        app_file.open("/root.txt", "w", error);
        app_file.write("I am root!", error);
        app_file.close(error);
        app_file.open("/dir_foo/foo.txt", "w", error);
        app_file.write("I am foo!", error);
        app_file.close(error);
        app_file.open("/dir_bar/bar.txt", "w", error);
        app_file.write("I am bar!", error);
        app_file.close(error);
        app_file.open("/dir_bar/new_bar.txt", "w", error);
        app_file.write("I am new bar!", error);
        app_file.close(error);
        app_file.open("/dir_foo/dir_deep/deep.txt", "w", error);
        app_file.write("I am deep!", error);
        app_file.close(error);
        fs.close(error);

        WHEN("we explore the tree") {
            littlefs::info info;
            std::uint32_t  index;
            std::uint32_t  dir_end;
            char           read_buffer[255];
            std::string    file;

            // Start at root
            fs.open("/", error);
            index   = fs.tell(error);        // index = 0 at the beginning of each folder
            dir_end = fs.read(info, error);  // info.name = '.'
            REQUIRE(dir_end == 1);
            dir_end = fs.read(info, error);  // info.name = '..'
            REQUIRE(dir_end == 1);
            dir_end = fs.read(info, error);  // info.name = "dir_bar", dir_end = 1 (not at the end of directory)
            REQUIRE(dir_end == 1);
            dir_end = fs.read(info, error);  // info.name = "dir_foo", dir_end = 1 (not at the end of directory)
            REQUIRE(dir_end == 1);
            dir_end = fs.read(info, error);  // info.name = "root.txt", dir_end = 1 (not at the end of directory)
            REQUIRE(dir_end == 1);
            dir_end = fs.read(info, error);  // info.name = "", dir_end = 0 (end of directory)
            REQUIRE(dir_end == 0);
            file = info.name;
            REQUIRE(file.empty());
            (void)index;  // Suppress warning
            // Redo the same, start from dir_bar, then dir_foo, then dir_deep

            // There is no need to open a directory, we still need the full path
            app_file.open("/dir_foo/dir_deep/deep.txt", "r", error);
            app_file.read(read_buffer, app_file.size(error), error);
            file = read_buffer;
            REQUIRE(file == "I am deep!");
        }
    }
    fs.unmount();
}
// NOLINTEND
