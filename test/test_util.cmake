find_package(Catch2 REQUIRED)

set(EXECUTABLE test_util)
add_executable(${EXECUTABLE} "")

target_sources(${EXECUTABLE}
    PRIVATE
    ${CMAKE_CURRENT_LIST_DIR}/catch2_main.cpp
    ${CMAKE_CURRENT_LIST_DIR}/test_circular_buffer.cpp
    ${CMAKE_CURRENT_LIST_DIR}/test_job_scheduler.cpp
    ${CMAKE_CURRENT_LIST_DIR}/test_logger.cpp
    ${CMAKE_CURRENT_LIST_DIR}/test_state_machine.cpp
    ${CMAKE_CURRENT_LIST_DIR}/test_time.cpp
    ${CMAKE_CURRENT_LIST_DIR}/test_littlefs.cpp
    ${CMAKE_CURRENT_LIST_DIR}/motor_state_machine.h
    )

target_include_directories(${EXECUTABLE}
    PRIVATE
    ${CMAKE_CURRENT_LIST_DIR}/vendor/fakeit/catch
    )

target_link_libraries(${EXECUTABLE}
    PRIVATE
    ${LIBRARY_NAME}
    Catch2::Catch2
    )

target_compile_features(${EXECUTABLE} PRIVATE cxx_std_17)
