/**
 * Copyright 2023 Christian Daigneault-St-Arnaud
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
 * following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions
 *    and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the
 *    following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote
 *    products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
// NOLINTBEGIN

#include "catch2/catch.hpp"
#include "circular_buffer.h"

using namespace util::container;

// Defines
#define TYPE_CAPACITY template<typename data_type, std::uint32_t buffer_capacity>
#define INSTANCE      Circular_buffer<data_type, buffer_capacity>& buffer

// Prototype
TYPE_CAPACITY
void fill_buffer(INSTANCE);

TYPE_CAPACITY
void overwrite_buffer(INSTANCE, std::uint32_t nb_write);

TYPE_CAPACITY
void set_cursor_to_position(INSTANCE, std::uint32_t position);

TYPE_CAPACITY
void print_buffer(INSTANCE);

// Tests parameters
#define DATA_TYPE std::uint8_t
#define CAPACITY  12

// Tests
TEST_CASE("Circular buffer", "[uint8_t]") {  // NOLINT

    Circular_buffer<DATA_TYPE, CAPACITY> buffer;

    SECTION("Initialization") {
        REQUIRE(CAPACITY == buffer.capacity());
        REQUIRE(0 == buffer.available());
        REQUIRE(true == buffer.empty());
    }

    SECTION("Push") {
        buffer.push(0xA5);
        REQUIRE(1 == buffer.available());
    }

    SECTION("Push and pop data") {
        DATA_TYPE value = 0x5A;
        buffer.push(value);
        REQUIRE(value == buffer.pop());
    }

    SECTION("Push and pop data with second version of pop") {
        DATA_TYPE value = 0xFA;
        buffer.push(value);

        auto out = buffer.pop();

        // Check value is still good after clearing all buffer's memory
        fill_buffer<DATA_TYPE, CAPACITY>(buffer);

        REQUIRE(*out == 0xFA);
    }

    SECTION("Buffer full") {
        fill_buffer<DATA_TYPE, CAPACITY>(buffer);

        REQUIRE(true == buffer.full());
        REQUIRE(CAPACITY == buffer.available());
    }

    SECTION("Read cursor > write cursor") {
        std::uint32_t nb_read = 4;
        std::uint32_t nb_add  = 2;

        REQUIRE(nb_add < nb_read);  // nb_read must be greater than nb_add to avoid overflow or full buffer at the end

        fill_buffer<DATA_TYPE, CAPACITY>(buffer);

        for (std::uint32_t i = 0; i < nb_read; ++i) {
            buffer.pop();
        }

        for (std::uint32_t i = 0; i < nb_add; ++i) {
            buffer.push(CAPACITY + i);
        }

        CHECK(false == buffer.full());
        REQUIRE((CAPACITY - nb_read + nb_add) == buffer.available());
        REQUIRE(nb_read == buffer.pop());
    }

    SECTION("Reset") {
        fill_buffer<DATA_TYPE, CAPACITY>(buffer);
        buffer.clear();

        REQUIRE(0 == buffer.available());
        REQUIRE(true == buffer.empty());
    }

    SECTION("Pop when it is empty") {
        DATA_TYPE value = 0xff;

        buffer.push(value);
        buffer.clear();

        // Overload
        auto out = buffer.pop();

        REQUIRE(out == std::nullopt);
    }

    SECTION("Push too much, overwrite") {
        std::uint32_t extra = 5;

        fill_buffer<DATA_TYPE, CAPACITY>(buffer);
        overwrite_buffer<DATA_TYPE, CAPACITY>(buffer, extra);

        CHECK(true == buffer.full());
        REQUIRE(CAPACITY == buffer.available());
    }

    SECTION("Read overwritten buffer") {
        std::uint32_t extra = 5;

        fill_buffer<DATA_TYPE, CAPACITY>(buffer);
        overwrite_buffer<DATA_TYPE, CAPACITY>(buffer, extra);

        // Read return the oldest value
        REQUIRE(extra == buffer.pop());
        REQUIRE(CAPACITY - 1 == buffer.available());
    }

    SECTION("Read overwritten buffer, check boundary") {
        std::uint32_t extra = CAPACITY + 3;

        fill_buffer<DATA_TYPE, CAPACITY>(buffer);
        overwrite_buffer<DATA_TYPE, CAPACITY>(buffer, extra);

        // Read return the oldest value
        REQUIRE(extra == buffer.pop());
    }

    SECTION("Read check boundary: read iterator cross buffer end") {
        std::uint32_t extra = CAPACITY - 1;

        fill_buffer<DATA_TYPE, CAPACITY>(buffer);
        overwrite_buffer<DATA_TYPE, CAPACITY>(buffer, extra);

        // Read return the oldest value (buffer_end)
        REQUIRE(CAPACITY - 1 == buffer.pop());
        // Cross buffer end
        REQUIRE(CAPACITY == buffer.pop());
    }

    SECTION("Full buffer from other position than 0") {
        std::uint32_t start_position = 4;

        set_cursor_to_position<DATA_TYPE, CAPACITY>(buffer, start_position);
        fill_buffer<DATA_TYPE, CAPACITY>(buffer);

        REQUIRE(true == buffer.full());
    }
}

// Function definition
TYPE_CAPACITY
void fill_buffer(INSTANCE) {
    for (std::uint32_t i = 0; i < CAPACITY; ++i) {
        buffer.push(i);
    }
}

TYPE_CAPACITY
void set_cursor_to_position(INSTANCE, std::uint32_t position) {
    std::uint8_t val = 0xA5;
    for (std::uint32_t i = 0; i < position; ++i) {
        buffer.push(val);
        buffer.pop();
    }
}

TYPE_CAPACITY
void overwrite_buffer(INSTANCE, std::uint32_t nb_write) {
    for (std::uint32_t i = 0; i < nb_write; ++i) {
        buffer.push(CAPACITY + i);
    }
}

TYPE_CAPACITY
void print_buffer(INSTANCE) {
    std::uint8_t val;
    for (std::uint32_t i = 0; i < CAPACITY; ++i) {
        val = buffer.pop();
        UNSCOPED_INFO("read = " + std::to_string(val) + "\r\n");
    }
    CHECK(false);
}

// NOLINTEND
