/**
 * Copyright 2023 Christian Daigneault-St-Arnaud
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
 * following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions
 *    and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the
 *    following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote
 *    products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
// NOLINTBEGIN

#include "catch2/catch.hpp"
#include "logger.h"
#include <vector>

using namespace util;
using namespace util::logger;

// Mock
/* Had problem with fakeit. Since const char* is used, the verification fail
 * because the string is not saved by fakeit.
 */
class mock_print : public Output {
public:
    mock_print() {
        reset_history();
    }
    void append(const char* log_message) override {
        print_history.emplace_back(log_message);
    }

    void reset_history() {
        print_history.clear();
    }

    std::vector<std::string> print_history;
};

// Used for timestamp
class Localtime : public time::Time {
public:
    void update_time() override {
        get_calendar_instance().time.hour = 11;
        get_calendar_instance().time.min  = 01;
        get_calendar_instance().time.sec  = 35;
        get_calendar_instance().time.ms   = 845;
    }

    void update_date() override {
        get_calendar_instance().date.year     = 2020;
        get_calendar_instance().date.month    = 3;
        get_calendar_instance().date.day      = 22;
        get_calendar_instance().date.week_day = util::time::monday;
    }

    void update_calendar() override {
        update_time();
        update_date();
    }

    void set_time(const util::time::Time_t& time_) override {}

    void set_date(const util::time::Date_t& date_) override {}

    void set_calendar(const util::time::Calendar_t& calendar_) override {}
};

// Test helper functions
void log_lvl_fatal_to_trace(Logger& logger);

SCENARIO("Logger levels", "[logger_levels]") {

    GIVEN("message are logged at different levels") {
        std::array<char, 128> buffer{};
        logger::Buffer_info_t buffer_info{.data = buffer.data(), .capacity = buffer.size()};

        mock_print debug_uart;
        Logger     log(buffer_info, debug_uart);

        WHEN("level is set to off") {
            log_lvl_fatal_to_trace(log);

            THEN("no messages are logged") {
                REQUIRE(log.get_level() == logger::Level::off);
                REQUIRE(debug_uart.print_history.empty());
            }
        }

        WHEN("level is set to fatal") {
            log.set_level(logger::Level::fatal);
            log_lvl_fatal_to_trace(log);

            THEN("only fatal messages are logged") {
                REQUIRE(log.get_level() == logger::Level::fatal);
                REQUIRE(debug_uart.print_history.size() == 2);
                REQUIRE(debug_uart.print_history[0] == "Fatal message");
                REQUIRE(debug_uart.print_history[1] == "Fatal message");
            }
        }

        WHEN("level is set to error") {
            log.set_level(logger::Level::error);
            log_lvl_fatal_to_trace(log);

            THEN("fatal & error messages are logged") {
                REQUIRE(log.get_level() == logger::Level::error);
                REQUIRE(debug_uart.print_history.size() == 4);
                REQUIRE(debug_uart.print_history[0] == "Fatal message");
                REQUIRE(debug_uart.print_history[1] == "Fatal message");
                REQUIRE(debug_uart.print_history[2] == "Error message");
                REQUIRE(debug_uart.print_history[3] == "Error message");
            }
        }

        WHEN("level is set to warning") {
            log.set_level(logger::Level::warning);
            log_lvl_fatal_to_trace(log);

            THEN("fatal & error & warning messages are logged") {
                REQUIRE(log.get_level() == logger::Level::warning);
                REQUIRE(debug_uart.print_history.size() == 6);
                REQUIRE(debug_uart.print_history[0] == "Fatal message");
                REQUIRE(debug_uart.print_history[1] == "Fatal message");
                REQUIRE(debug_uart.print_history[2] == "Error message");
                REQUIRE(debug_uart.print_history[3] == "Error message");
                REQUIRE(debug_uart.print_history[4] == "Warning message");
                REQUIRE(debug_uart.print_history[5] == "Warning message");
            }
        }

        WHEN("level is set to info") {
            log.set_level(logger::Level::info);
            log_lvl_fatal_to_trace(log);

            THEN("fatal & error & warning & info messages are logged") {
                REQUIRE(log.get_level() == logger::Level::info);
                REQUIRE(debug_uart.print_history.size() == 8);
                REQUIRE(debug_uart.print_history[0] == "Fatal message");
                REQUIRE(debug_uart.print_history[1] == "Fatal message");
                REQUIRE(debug_uart.print_history[2] == "Error message");
                REQUIRE(debug_uart.print_history[3] == "Error message");
                REQUIRE(debug_uart.print_history[4] == "Warning message");
                REQUIRE(debug_uart.print_history[5] == "Warning message");
                REQUIRE(debug_uart.print_history[6] == "Info message");
                REQUIRE(debug_uart.print_history[7] == "Info message");
            }
        }

        WHEN("level is set to debug") {
            log.set_level(logger::Level::debug);
            log_lvl_fatal_to_trace(log);

            THEN("fatal & error & warning & info & debug messages are logged") {
                REQUIRE(log.get_level() == logger::Level::debug);
                REQUIRE(debug_uart.print_history.size() == 10);
                REQUIRE(debug_uart.print_history[0] == "Fatal message");
                REQUIRE(debug_uart.print_history[1] == "Fatal message");
                REQUIRE(debug_uart.print_history[2] == "Error message");
                REQUIRE(debug_uart.print_history[3] == "Error message");
                REQUIRE(debug_uart.print_history[4] == "Warning message");
                REQUIRE(debug_uart.print_history[5] == "Warning message");
                REQUIRE(debug_uart.print_history[6] == "Info message");
                REQUIRE(debug_uart.print_history[7] == "Info message");
                REQUIRE(debug_uart.print_history[8] == "Debug message");
                REQUIRE(debug_uart.print_history[9] == "Debug message");
            }
        }

        WHEN("level is set to trace") {
            log.set_level(logger::Level::trace);
            log_lvl_fatal_to_trace(log);

            THEN("all messages are logged") {
                REQUIRE(log.get_level() == logger::Level::trace);
                REQUIRE(debug_uart.print_history.size() == 12);
                REQUIRE(debug_uart.print_history[0] == "Fatal message");
                REQUIRE(debug_uart.print_history[1] == "Fatal message");
                REQUIRE(debug_uart.print_history[2] == "Error message");
                REQUIRE(debug_uart.print_history[3] == "Error message");
                REQUIRE(debug_uart.print_history[4] == "Warning message");
                REQUIRE(debug_uart.print_history[5] == "Warning message");
                REQUIRE(debug_uart.print_history[6] == "Info message");
                REQUIRE(debug_uart.print_history[7] == "Info message");
                REQUIRE(debug_uart.print_history[8] == "Debug message");
                REQUIRE(debug_uart.print_history[9] == "Debug message");
                REQUIRE(debug_uart.print_history[10] == "Trace message");
                REQUIRE(debug_uart.print_history[11] == "Trace message");
            }
        }

        WHEN("level is set to all") {
            log.set_level(logger::Level::all);
            log_lvl_fatal_to_trace(log);

            THEN("all messages are logged") {
                REQUIRE(log.get_level() == logger::Level::all);
                REQUIRE(debug_uart.print_history.size() == 12);
                REQUIRE(debug_uart.print_history[0] == "Fatal message");
                REQUIRE(debug_uart.print_history[1] == "Fatal message");
                REQUIRE(debug_uart.print_history[2] == "Error message");
                REQUIRE(debug_uart.print_history[3] == "Error message");
                REQUIRE(debug_uart.print_history[4] == "Warning message");
                REQUIRE(debug_uart.print_history[5] == "Warning message");
                REQUIRE(debug_uart.print_history[6] == "Info message");
                REQUIRE(debug_uart.print_history[7] == "Info message");
                REQUIRE(debug_uart.print_history[8] == "Debug message");
                REQUIRE(debug_uart.print_history[9] == "Debug message");
                REQUIRE(debug_uart.print_history[10] == "Trace message");
                REQUIRE(debug_uart.print_history[11] == "Trace message");
            }
        }
    }
}

SCENARIO("Logger", "[logger]") {

    mock_print debug_uart;

    GIVEN("Log variable value") {

        std::array<char, 128> buffer{};
        logger::Buffer_info_t buffer_info{.data = buffer.data(), .capacity = buffer.size()};
        Logger                log(buffer_info, debug_uart, logger::Level::all);

        WHEN(" we want to log count value ") {

            std::uint8_t count = 12;
            log.log(logger::Level::info, "Count value is : %d", count);

            THEN("count value is logged") {
                REQUIRE(debug_uart.print_history.size() == 1);
                REQUIRE(debug_uart.print_history[0] == "Count value is : 12");
            }
        }

        WHEN(" we want to log 2 values ") {

            std::uint8_t param1 = 12;
            std::uint8_t param2 = 24;
            log.log(logger::Level::info, "Parameters are: %d & %d", param1, param2);

            THEN("the 2 parameters are logged") {
                REQUIRE(debug_uart.print_history.size() == 1);
                REQUIRE(debug_uart.print_history[0] == "Parameters are: 12 & 24");
            }
        }
    }

    GIVEN("Logger name in log") {
        std::array<char, 128> buffer{};
        logger::Buffer_info_t buffer_info{.data = buffer.data(), .capacity = buffer.size()};
        Logger                log(buffer_info, debug_uart, logger::Level::all, "Controller");

        WHEN(" anything is logged ") {

            log.enable_name();
            log.log(logger::Level::fatal, "Temperature too high");

            log.disable_name();
            log.log(logger::Level::fatal, "Temperature too high");

            THEN("logger's name is added to message") {
                REQUIRE(debug_uart.print_history.size() == 2);
                REQUIRE(debug_uart.print_history[0] == "Controller : Temperature too high");
                REQUIRE(debug_uart.print_history[1] == "Temperature too high");
            }
        }
    }

    GIVEN("Message is too big") {

        std::array<char, 33>  buffer{};
        logger::Buffer_info_t buffer_info{.data = buffer.data(), .capacity = buffer.size()};
        Logger                log(buffer_info, debug_uart, logger::Level::all, "Name");

        WHEN("logging a long string") {

            log.enable_name();
            log.log(logger::Level::info, "A very long string bigger than the output buffer");

            THEN("only the maximum size is logged") {
                REQUIRE(debug_uart.print_history.size() == 1);
                REQUIRE(debug_uart.print_history[0] == "Name : A very long string bigger");
            }
        }
    }

    GIVEN("Add timestamp to log") {

        Localtime time;

        std::array<char, 128> buffer{};
        logger::Buffer_info_t buffer_info{.data = buffer.data(), .capacity = buffer.size()};
        Logger                log(buffer_info, debug_uart, logger::Level::all, "Name");

        WHEN(" format is YYYY-MM-DD") {
            log.config_timestamp(time, logger::Timestamp_format::yyyy_mm_dd);
            log.enable_timestamp();

            log.log(logger::Level::info, "Info message");

            THEN("timestamp is added before the message") {
                REQUIRE(debug_uart.print_history.size() == 1);
                REQUIRE(debug_uart.print_history[0] == "2020-03-22 : Info message");
            }
        }

        WHEN(" format is HH-MM-SS") {
            log.config_timestamp(time, logger::Timestamp_format::hh_mm_ss);
            log.enable_timestamp();

            log.log(logger::Level::info, "Info message");

            THEN("timestamp is added before the message") {
                REQUIRE(debug_uart.print_history.size() == 1);
                REQUIRE(debug_uart.print_history[0] == "11:01:35 : Info message");
            }
        }

        WHEN(" format is HH-MM-SS-MMMM") {
            log.config_timestamp(time, logger::Timestamp_format::hh_mm_ss_mmm);
            log.enable_timestamp();

            log.log(logger::Level::info, "Info message");

            THEN("timestamp is added before the message") {
                REQUIRE(debug_uart.print_history.size() == 1);
                REQUIRE(debug_uart.print_history[0] == "11:01:35,845 : Info message");
            }
        }

        WHEN(" format is YYYY-MM-DD HH-MM-SS") {
            log.config_timestamp(time, logger::Timestamp_format::yyyy_mm_dd_hh_mm_ss);
            log.enable_timestamp();

            log.log(logger::Level::info, "Info message");

            THEN("timestamp is added before the message") {
                REQUIRE(debug_uart.print_history.size() == 1);
                REQUIRE(debug_uart.print_history[0] == "2020-03-22 11:01:35 : Info message");
            }
        }

        WHEN(" format is YYYY-MM-DD HH-MM-SS-MMMM") {
            log.config_timestamp(time, logger::Timestamp_format::yyyy_mm_dd_hh_mm_ss_mmm);
            log.enable_timestamp();

            log.log(logger::Level::info, "Info message");

            THEN("timestamp is added before the message") {
                REQUIRE(debug_uart.print_history.size() == 1);
                REQUIRE(debug_uart.print_history[0] == "2020-03-22 11:01:35,845 : Info message");
            }
        }
    }

    GIVEN("Add timestamp & name to log") {

        Localtime             time;
        std::array<char, 128> buffer{};
        logger::Buffer_info_t buffer_info{.data = buffer.data(), .capacity = buffer.size()};
        Logger                log(buffer_info, debug_uart, logger::Level::all, "Console");

        log.config_timestamp(time, logger::Timestamp_format::yyyy_mm_dd_hh_mm_ss_mmm);
        log.enable_timestamp();
        log.enable_name();

        WHEN(" timestamp format is YYYY-MM-DD HH-MM-SS-MMMM & name is Console") {
            log.log(logger::Level::info, "Info message");

            THEN("timestamp & name are added before the message") {
                REQUIRE(debug_uart.print_history.size() == 1);
                REQUIRE(debug_uart.print_history[0] == "2020-03-22 11:01:35,845 Console : Info message");
            }
        }
    }

    GIVEN("Enable / disable timestamp") {

        Localtime             time;
        std::array<char, 128> buffer{};
        logger::Buffer_info_t buffer_info{.data = buffer.data(), .capacity = buffer.size()};
        Logger                log(buffer_info, debug_uart, logger::Level::all, "Name");

        log.config_timestamp(time, logger::Timestamp_format::yyyy_mm_dd_hh_mm_ss_mmm);

        WHEN("timestamp is enabled & disabled") {

            log.enable_timestamp();
            log.log(logger::Level::info, "Info message");

            log.disable_timestamp();
            log.log(logger::Level::info, "Info message");

            THEN("timestamp is added & not added respectively") {
                REQUIRE(debug_uart.print_history.size() == 2);
                REQUIRE(debug_uart.print_history[0] == "2020-03-22 11:01:35,845 : Info message");
                REQUIRE(debug_uart.print_history[1] == "Info message");
            }
        }
    }

    GIVEN("Add log level") {

        std::array<char, 128> buffer{};
        logger::Buffer_info_t buffer_info{.data = buffer.data(), .capacity = buffer.size()};
        Logger                log(buffer_info, debug_uart, logger::Level::all, "Name");

        WHEN("enabled & disabled") {
            log.enable_level();
            log.info("message");

            log.disable_level();
            log.info("message");

            THEN("Log level is added & not added the message") {
                REQUIRE(debug_uart.print_history.size() == 2);
                REQUIRE(debug_uart.print_history[0] == "INFO : message");
                REQUIRE(debug_uart.print_history[1] == "message");
            }
        }

        WHEN("all log level are used") {

            log.enable_level();

            log.log(logger::Level::off, "message");
            log.fatal("message");
            log.error("message");
            log.warning("message");
            log.info("message");
            log.debug("message");
            log.trace("message");
            log.log(logger::Level::all, "message");

            THEN("Log level is added before the message") {
                REQUIRE(debug_uart.print_history.size() == 8);
                REQUIRE(debug_uart.print_history[0] == "OFF : message");
                REQUIRE(debug_uart.print_history[1] == "FATAL : message");
                REQUIRE(debug_uart.print_history[2] == "ERROR : message");
                REQUIRE(debug_uart.print_history[3] == "WARNING : message");
                REQUIRE(debug_uart.print_history[4] == "INFO : message");
                REQUIRE(debug_uart.print_history[5] == "DEBUG : message");
                REQUIRE(debug_uart.print_history[6] == "TRACE : message");
                REQUIRE(debug_uart.print_history[7] == "ALL : message");
            }
        }
    }
}

// Private functions
void log_lvl_fatal_to_trace(Logger& logger) {
    logger.log(logger::Level::fatal, "Fatal message");
    logger.fatal("Fatal message");
    logger.log(logger::Level::error, "Error message");
    logger.error("Error message");
    logger.log(logger::Level::warning, "Warning message");
    logger.warning("Warning message");
    logger.log(logger::Level::info, "Info message");
    logger.info("Info message");
    logger.log(logger::Level::debug, "Debug message");
    logger.debug("Debug message");
    logger.log(logger::Level::trace, "Trace message");
    logger.trace("Trace message");
}
// NOLINTEND
