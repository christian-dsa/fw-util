/**
 * Copyright 2023 Christian Daigneault-St-Arnaud
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
 * following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions
 *    and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the
 *    following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote
 *    products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef MOTOR_STATE_MACHINE_H
#define MOTOR_STATE_MACHINE_H

#include "state_machine.h"
#include <string>

class Drive {
public:
    virtual void test(std::string message) = 0;
    virtual ~Drive()                       = default;

    Drive()                        = default;  // Default constructor
    Drive(const Drive&)            = delete;   // Non construction-copyable
    Drive& operator=(const Drive&) = delete;   // non copyable
    Drive(Drive&&)                 = delete;   // non movable
    Drive& operator=(Drive&&)      = delete;   // No move assignment operator
};

class Motor_fsm {
public:
    explicit Motor_fsm(Drive& drive)
    : sm(),
      idle(drive, State_name::state_idle),
      running(drive, State_name::state_running, speed),
      stop(drive, State_name::state_stop),
      error(drive, State_name::state_error) {
        sm.init(idle, matrix.begin(), matrix.end());
    };

    // States list
    enum State_name : util::sm::state_name_t { state_init, state_idle, state_running, state_stop, state_error };

    // Events list
    enum Event_name : util::sm::event_name_t {
        event_none,
        event_motor_stopped,
        event_idle,
        event_change_speed,
        event_halt,
        event_trig_error
    };

    void change_speed(uint32_t new_speed) {
        speed = new_speed;

        if (!sm.process(Event_name::event_change_speed)) {
            sm.handle_error(&error);
        }
    }

    void halt() {
        if (!sm.process(Event_name::event_halt)) {
            sm.handle_error(&error);
        }
    }

    void trig_error() {
        if (!sm.process(Event_name::event_trig_error)) {
            sm.handle_error(&error);
        }
    }

    util::sm::state_name_t get_current_state_name() {
        return sm.get_current_state()->get_name();
    }

private:
    // Event object
    class Motor_event_data {
    public:
        Motor_event_data() = default;

    private:
        uint32_t speed{0};
    };

    // States object
    class Idle : public util::sm::State {
    public:
        Idle(Drive& drive, util::sm::state_name_t state_name)
        : State(state_name),
          drive(&drive){};

        std::optional<util::sm::event_name_t> execute() override {
            drive->test("Execute idle");
            return std::nullopt;
        };

    private:
        Drive* drive;
    };

    class Running : public util::sm::State {
    public:
        Running(Drive& drive, util::sm::state_name_t state_name, std::uint8_t& new_speed)
        : State(state_name),
          drive(&drive),
          speed(&new_speed){};

        std::optional<util::sm::event_name_t> execute() override {
            drive->test("Execute running " + std::to_string(*speed));
            return std::nullopt;
        };

    private:
        Drive*        drive;
        std::uint8_t* speed;
    };

    class Stop : public util::sm::State {
    public:
        Stop(Drive& drive, util::sm::state_name_t state_name)
        : State(state_name),
          drive(&drive){};

        std::optional<util::sm::event_name_t> execute() override {
            drive->test("Execute stop");
            return Event_name::event_motor_stopped;
        };

    private:
        Drive* drive;
    };

    class Error : public util::sm::State {
    public:
        Error(Drive& drive, util::sm::state_name_t state_name)
        : State(state_name),
          drive(&drive){};

        std::optional<util::sm::event_name_t> execute() override {
            drive->test("Execute error");
            return std::nullopt;
        };

    private:
        Drive* drive;
    };

    // State machine
    util::sm::State_machine sm;

    // States declaration
    Idle    idle;
    Running running;
    Stop    stop;
    Error   error;

    // State parameter.
    std::uint8_t speed{0};

    // State matrix
    static constexpr std::uint8_t motor_fsm_matrix_size = 6;

    using state_matrix = std::array<util::sm::State_machine::State_transition, motor_fsm_matrix_size>;

    state_matrix matrix = {
        {{&idle, Event_name::event_none, &idle},
         {&idle, Event_name::event_change_speed, &running},
         {&running, Event_name::event_halt, &stop},
         {&running, Event_name::event_change_speed, &running},
         {&stop, Event_name::event_motor_stopped, &idle},
         {&stop, Event_name::event_change_speed, &running}}
    };
};

#endif /* MOTOR_STATE_MACHINE_H */
