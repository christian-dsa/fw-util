/**
 * Copyright 2023 Christian Daigneault-St-Arnaud
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
 * following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions
 *    and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the
 *    following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote
 *    products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
// NOLINTBEGIN

#include "catch2/catch.hpp"
#include "time_util.h"
using namespace util::time;

class Rtc : public Time {
public:
    void update_time() override {
        get_calendar_instance().time.hour = 11;
        get_calendar_instance().time.min  = 01;
        get_calendar_instance().time.sec  = 35;
        get_calendar_instance().time.ms   = get_calendar_instance().time.ms;
    }

    void update_date() override {
        get_calendar_instance().date.year     = 2020;
        get_calendar_instance().date.month    = 3;
        get_calendar_instance().date.day      = 22;
        get_calendar_instance().date.week_day = util::time::monday;
    }

    void update_calendar() override {
        update_time();
        update_date();
    }

    void set_ms(const std::uint16_t ms) {
        get_calendar_instance().time.ms = ms;
    }

    void set_time(const util::time::Time_t& time_) override {}

    void set_date(const util::time::Date_t& date_) override {}

    void set_calendar(const util::time::Calendar_t& calendar_) override {}
};

SCENARIO("Time class", "[time]") {

    GIVEN("Time comes from Real Time Clock") {

        Rtc rtc;

        rtc.update_time();
        rtc.update_date();

        rtc.set_ms(845);

        // String buffer
        constexpr std::size_t          str_max_size = 32;
        std::array<char, str_max_size> str_buffer{};

        WHEN("application request time") {
            util::time::Time_t t = rtc.get_time();

            THEN("application get a copy of time_t struct ") {
                REQUIRE(t.hour == 11);
                REQUIRE(t.min == 01);
                REQUIRE(t.sec == 35);
                REQUIRE(t.ms == 845);
            }
        }

        WHEN("application request string with date format yyyy-mm-dd") {
            Date_t      date     = rtc.get_date();
            std::size_t str_size = format_date_yyyy_mm_dd(date, str_buffer.begin(), str_buffer.end());

            THEN("a string with requested format is received") {
                std::string_view str{str_buffer.data(), str_size};
                REQUIRE(str == "2020-03-22");
            }
        }

        WHEN("application request string with time format hh:mm:ss") {
            util::time::Time_t localtime = rtc.get_time();
            std::size_t        str_size  = format_time_hh_mm_ss(localtime, str_buffer.begin(), str_buffer.end());

            THEN("a string with requested format is received") {
                std::string_view str{str_buffer.data(), str_size};
                REQUIRE(str == "11:01:35");
            }
        }

        WHEN("application request string with time format hh:mm:ss,mmmm, ms = 5") {
            rtc.set_ms(5);
            util::time::Time_t localtime = rtc.get_time();
            std::size_t        str_size  = format_time_hh_mm_ss_mmm(localtime, str_buffer.begin(), str_buffer.end());

            THEN("a string with requested format is received") {
                std::string_view str{str_buffer.data(), str_size};
                REQUIRE(str == "11:01:35,005");
            }
        }

        WHEN("application request string with time format hh:mm:ss,mmmm, ms = 50") {
            rtc.set_ms(50);
            util::time::Time_t localtime = rtc.get_time();
            std::size_t        str_size  = format_time_hh_mm_ss_mmm(localtime, str_buffer.begin(), str_buffer.end());

            THEN("a string with requested format is received") {
                std::string_view str{str_buffer.data(), str_size};
                REQUIRE(str == "11:01:35,050");
            }
        }

        WHEN("application request string with time format hh:mm:ss,mmmm, ms = 505") {
            rtc.set_ms(505);
            util::time::Time_t localtime = rtc.get_time();
            std::size_t        str_size  = format_time_hh_mm_ss_mmm(localtime, str_buffer.begin(), str_buffer.end());

            THEN("a string with requested format is received") {
                std::string_view str{str_buffer.data(), str_size};
                REQUIRE(str == "11:01:35,505");
            }
        }

        WHEN("application request string with date & time format yyyy-mm-dd hh:mm:ss") {
            Calendar_t  calendar = rtc.get_calendar();
            std::size_t str_size = format_date_time_yyyy_mm_dd_hh_mm_ss(calendar, str_buffer.begin(), str_buffer.end());

            THEN("a string with requested format is received") {
                std::string_view str{str_buffer.data(), str_size};
                REQUIRE(str == "2020-03-22 11:01:35");
            }
        }

        WHEN("application request string with date & time format yyyy-mm-dd hh:mm:ss,mmm") {
            Calendar_t  calendar = rtc.get_calendar();
            std::size_t str_size =
                format_date_time_yyyy_mm_dd_hh_mm_ss_mmm(calendar, str_buffer.begin(), str_buffer.end());

            THEN("a string with requested format is received") {
                std::string_view str{str_buffer.data(), str_size};
                REQUIRE(str == "2020-03-22 11:01:35,845");
            }
        }
    }

    GIVEN("Operator + & - are overloaded") {

        WHEN("we add 4:15:29:550 to 11:10:50:450") {
            util::time::Time_t time1 = {.hour = 4, .min = 15, .sec = 29, .ms = 550};

            util::time::Time_t time2 = {.hour = 11, .min = 10, .sec = 50, .ms = 450};

            util::time::Time_t time_sum = time1 + time2;

            THEN("the result is 15:26:20:000") {
                REQUIRE(time_sum.hour == 15);
                REQUIRE(time_sum.min == 26);
                REQUIRE(time_sum.sec == 20);
                REQUIRE(time_sum.ms == 0);
            }
        }

        WHEN("we add 4:15:30:500 to 11:10:30:500") {
            util::time::Time_t time1 = {.hour = 4, .min = 15, .sec = 30, .ms = 500};

            util::time::Time_t time2 = {.hour = 11, .min = 10, .sec = 29, .ms = 500};

            util::time::Time_t time_sum = time1 + time2;

            THEN("the result is 15:26:00:000") {
                REQUIRE(time_sum.hour == 15);
                REQUIRE(time_sum.min == 26);
                REQUIRE(time_sum.sec == 0);
                REQUIRE(time_sum.ms == 0);
            }
        }

        WHEN("we add 18:15:29:550 to 12:10:50:460") {
            util::time::Time_t time1 = {.hour = 18, .min = 15, .sec = 29, .ms = 550};

            util::time::Time_t time2 = {.hour = 12, .min = 10, .sec = 50, .ms = 460};

            util::time::Time_t time_sum = time1 + time2;

            THEN("the result is 30:26:20:010") {
                REQUIRE(time_sum.hour == 30);
                REQUIRE(time_sum.min == 26);
                REQUIRE(time_sum.sec == 20);
                REQUIRE(time_sum.ms == 10);
            }
        }

        WHEN("we subtract 4:15:29:550 from 15:26:20:000") {
            util::time::Time_t time1 = {.hour = 4, .min = 15, .sec = 29, .ms = 550};

            util::time::Time_t time2 = {.hour = 15, .min = 26, .sec = 20, .ms = 0};

            util::time::Time_t time_sub = time2 - time1;

            THEN("the result is 11:10:50:450") {
                REQUIRE(time_sub.hour == 11);
                REQUIRE(time_sub.min == 10);
                REQUIRE(time_sub.sec == 50);
                REQUIRE(time_sub.ms == 450);
            }
        }

        WHEN("we subtract 10:15:00:500 from 5:26:20:000") {
            util::time::Time_t time1 = {.hour = 10, .min = 15, .sec = 0, .ms = 500};

            util::time::Time_t time2 = {.hour = 5, .min = 26, .sec = 20, .ms = 0};

            util::time::Time_t time_sub = time2 - time1;

            THEN("the result is -5:11:19:500") {
                REQUIRE(time_sub.hour == -5);
                REQUIRE(time_sub.min == 11);
                REQUIRE(time_sub.sec == 19);
                REQUIRE(time_sub.ms == 500);
            }
        }
    }
}
// NOLINTEND
