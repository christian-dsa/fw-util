/**
 * Copyright 2023 Christian Daigneault-St-Arnaud
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
 * following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions
 *    and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the
 *    following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote
 *    products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef TIME_UTIL_H
#define TIME_UTIL_H

#include <charconv>
#include <cstdint>

namespace util::time {

// Constants
static constexpr const char date_separation      = '-';
static constexpr const char date_time_separation = ' ';
static constexpr const char time_separation      = ':';
static constexpr const char ms_separation        = ',';

static constexpr std::uint32_t ms_in_sec   = 1000;
static constexpr std::uint32_t sec_in_min  = 60;
static constexpr std::uint32_t min_in_hour = 60;

// Type
enum Week_day : std::uint8_t {
    none      = 0,
    monday    = 1,
    tuesday   = 2,
    wednesday = 3,
    thursday  = 4,
    friday    = 5,
    saturday  = 6,
    sunday    = 7
};

struct Date_t {
    std::uint16_t year;
    std::uint8_t  month;
    std::uint8_t  day;
    std::uint8_t  week_day;
};

struct Time_t {
    std::int32_t hour;  // hour can be negative or go beyond 24 when used to represent a difference between
                        // two time point.
    std::uint8_t  min;
    std::uint8_t  sec;
    std::uint16_t ms;
};

struct Calendar_t {
    Date_t date;
    Time_t time;
};

// Operator overloading
inline Time_t operator+(const Time_t& time_a, const Time_t& time_b) {
    Time_t sum{};

    const std::uint32_t temp_ms   = time_a.ms + time_b.ms;
    const std::uint32_t temp_sec  = time_a.sec + time_b.sec + temp_ms / ms_in_sec;
    const std::uint32_t temp_min  = time_a.min + time_b.min + temp_sec / 60U;
    const std::uint32_t temp_hour = time_a.hour + time_b.hour + temp_min / 60U;

    sum.ms   = temp_ms % ms_in_sec;
    sum.sec  = temp_sec % sec_in_min;
    sum.min  = temp_min % min_in_hour;
    sum.hour = static_cast<std::int32_t>(temp_hour);

    return sum;
}

// Operator overloading
inline Time_t operator-(const Time_t& time_a, const Time_t& time_b) {
    Time_t sub{};

    // Add one of the next unit (add 1 sec to temp_ms) and subtract it when this unit is handled. This way, when
    // x > y, the next unit will do -1.
    const auto temp_ms  = static_cast<std::int32_t>(time_a.ms - time_b.ms + ms_in_sec);
    const auto temp_sec = static_cast<std::int32_t>(time_a.sec - time_b.sec - (1 - temp_ms / ms_in_sec) + sec_in_min);
    const auto temp_min =
        static_cast<std::int32_t>(time_a.min - time_b.min - (1 - temp_sec / sec_in_min) + min_in_hour);
    const auto temp_hour = static_cast<std::int32_t>(time_a.hour - time_b.hour - (1 - temp_min / min_in_hour) + 0U);

    sub.ms   = temp_ms % ms_in_sec;
    sub.sec  = temp_sec % sec_in_min;
    sub.min  = temp_min % min_in_hour;
    sub.hour = temp_hour;

    return sub;
}

// Time format
enum class Digits { digits_2 = 2, digits_3 = 1, digits_4 = 0 };

inline std::size_t to_str_n_digits(int value, char* out_begin, char* out_end, Digits digits) {
    static constexpr std::size_t                            lookup_size = 4;
    static constexpr std::array<std::uint16_t, lookup_size> lookup{1000, 100, 10, 0};
    static constexpr std::size_t                            decimal = 10;

    std::size_t size     = 0;
    char*       write_it = out_begin;
    for (const auto* it = lookup.begin() + static_cast<std::size_t>(digits); it != lookup.end(); ++it) {
        if ((value < *it) && (write_it != out_end)) {
            *write_it = '0';
            ++write_it;
            ++size;
        } else {
            auto [ptr, _] = std::to_chars(write_it, out_end, value, decimal);
            size          = ptr - out_begin;
            break;
        }
    }
    return size;
}

// Format methods are meant to be used to represent time of day. They are not meant to represent a difference
// between two time point. Thus, negative hours, or any value that goes beyond 24 hours are not handled.
inline std::size_t format_date_yyyy_mm_dd(Date_t& date, char* out_begin, char* out_end) {
    std::size_t size    = to_str_n_digits(date.year, out_begin, out_end, Digits::digits_4);
    *(out_begin + size) = date_separation;
    ++size;
    size                += to_str_n_digits(date.month, out_begin + size, out_end, Digits::digits_2);
    *(out_begin + size) = date_separation;
    ++size;
    size += to_str_n_digits(date.day, out_begin + size, out_end, Digits::digits_2);

    return size;
}

inline std::size_t format_time_hh_mm_ss(Time_t& time, char* out_begin, char* out_end) {
    std::size_t size    = to_str_n_digits(time.hour, out_begin, out_end, Digits::digits_2);
    *(out_begin + size) = time_separation;
    ++size;
    size                += to_str_n_digits(time.min, out_begin + size, out_end, Digits::digits_2);
    *(out_begin + size) = time_separation;
    ++size;
    size += to_str_n_digits(time.sec, out_begin + size, out_end, Digits::digits_2);

    return size;
}

inline std::size_t format_time_hh_mm_ss_mmm(Time_t& time, char* out_begin, char* out_end) {
    std::size_t size    = format_time_hh_mm_ss(time, out_begin, out_end);
    *(out_begin + size) = ms_separation;
    ++size;
    size += to_str_n_digits(time.ms, out_begin + size, out_end, Digits::digits_3);

    return size;
}

inline std::size_t format_date_time_yyyy_mm_dd_hh_mm_ss(Calendar_t& calendar, char* out_begin, char* out_end) {
    std::size_t size    = format_date_yyyy_mm_dd(calendar.date, out_begin, out_end);
    *(out_begin + size) = date_time_separation;
    ++size;
    size += format_time_hh_mm_ss(calendar.time, out_begin + size, out_end);

    return size;
}

inline std::size_t format_date_time_yyyy_mm_dd_hh_mm_ss_mmm(Calendar_t& calendar, char* out_begin, char* out_end) {
    std::size_t size    = format_date_time_yyyy_mm_dd_hh_mm_ss(calendar, out_begin, out_end);
    *(out_begin + size) = ms_separation;
    ++size;
    size += to_str_n_digits(calendar.time.ms, out_begin + size, out_end, Digits::digits_3);

    return size;
}

// Class
class Time {
public:
    Time()
    : calendar({0, 0, 0, 0, 0, 0, 0, 0}) {}

    virtual void update_time() = 0;

    virtual void update_date() = 0;

    virtual void update_calendar() = 0;

    virtual void set_time(const Time_t& time) = 0;

    virtual void set_date(const Date_t& date) = 0;

    virtual void set_calendar(const Calendar_t& calendar) = 0;

    Time_t get_time() {
        update_time();

        return calendar.time;
    }

    Date_t get_date() {
        update_date();

        return calendar.date;
    }

    Calendar_t get_calendar() {
        update_time();
        update_date();

        return calendar;
    }

    virtual ~Time() = default;

    Time(const Time&)            = delete;  // Non construction-copyable
    Time& operator=(const Time&) = delete;  // non copyable
    Time(Time&&)                 = delete;  // non movable
    Time& operator=(Time&&)      = delete;  // No move assignment operator

protected:
    Calendar_t& get_calendar_instance() {
        return calendar;
    }

private:
    Calendar_t calendar;
};
}  // namespace util::time

#endif /* TIME_UTIL_H */
