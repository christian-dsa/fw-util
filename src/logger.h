/**
 * Copyright 2023 Christian Daigneault-St-Arnaud
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
 * following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions
 *    and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the
 *    following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote
 *    products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef LOGGER_H
#define LOGGER_H

#include "time_util.h"
#include <array>
#include <cstdarg>
#include <cstdio>

namespace util::logger {

// Disable all flags related to the printf format which is used for this logger.
// NOLINTBEGIN(cert-dcl50-cpp)
// NOLINTBEGIN(cppcoreguidelines-pro-type-vararg)
// NOLINTBEGIN(hicpp-vararg)
// NOLINTBEGIN(cppcoreguidelines-pro-bounds-array-to-pointer-decay)
// NOLINTBEGIN(hicpp-no-array-decay)

// Buffer. This is done to avoid having a template for the whole class
struct Buffer_info_t {
    char*       data;
    std::size_t capacity;
};

// Interface
class Output {
public:
    virtual void append(const char* log_message) = 0;

    virtual ~Output() = default;

    Output()                         = default;  // Default constructor
    Output(const Output&)            = delete;   // Non construction-copyable
    Output& operator=(const Output&) = delete;   // non copyable
    Output(Output&&)                 = delete;   // non movable
    Output& operator=(Output&&)      = delete;   // No move assignment operator
};

// Type
enum class Level : std::uint8_t { off = 0, fatal = 1, error = 2, warning = 3, info = 4, debug = 5, trace = 6, all = 7 };

enum class Timestamp_format {
    yyyy_mm_dd,
    hh_mm_ss,
    hh_mm_ss_mmm,
    yyyy_mm_dd_hh_mm_ss,
    yyyy_mm_dd_hh_mm_ss_mmm,
};

// Logger
class Logger {
public:
    Logger(logger::Buffer_info_t& buffer_info,
           logger::Output&        output,
           logger::Level          level = logger::Level::off,
           std::string_view       name  = "")
    : buffer_info(&buffer_info),
      log_output(buffer_info.data),
      out(&output),
      name(name),
      priority_level(level) {}

    void config_timestamp(util::time::Time&        instance,
                          logger::Timestamp_format format = logger::Timestamp_format::yyyy_mm_dd_hh_mm_ss_mmm) {
        time             = &instance;
        timestamp_format = format;
    }

    void enable_timestamp() {
        add_timestamp = true;
    }

    void disable_timestamp() {
        add_timestamp = false;
    }

    void set_level(logger::Level level) {
        priority_level = level;
    }

    logger::Level get_level() {
        return priority_level;
    }

    void enable_name() {
        add_name = true;
    }

    void disable_name() {
        add_name = false;
    }

    void enable_level() {
        add_level = true;
    }

    void disable_level() {
        add_level = false;
    }

    void log(logger::Level msg_level, const char* message, ...) {
        std::va_list args;
        va_start(args, message);
        log_with_args(msg_level, message, args);
        va_end(args);
    }

    void fatal(const char* message, ...) {
        std::va_list args;
        va_start(args, message);
        log_with_args(logger::Level::fatal, message, args);
        va_end(args);
    }

    void error(const char* message, ...) {
        std::va_list args;
        va_start(args, message);
        log_with_args(logger::Level::error, message, args);
        va_end(args);
    }

    void warning(const char* message, ...) {
        std::va_list args;
        va_start(args, message);
        log_with_args(logger::Level::warning, message, args);
        va_end(args);
    }

    void info(const char* message, ...) {
        std::va_list args;
        va_start(args, message);
        log_with_args(logger::Level::info, message, args);
        va_end(args);
    }

    void debug(const char* message, ...) {
        std::va_list args;
        va_start(args, message);
        log_with_args(logger::Level::debug, message, args);
        va_end(args);
    }

    void trace(const char* message, ...) {
        std::va_list args;
        va_start(args, message);
        log_with_args(logger::Level::trace, message, args);
        va_end(args);
    }

private:
    void log_with_args(logger::Level msg_level, const char* message, std::va_list args) {
        if (msg_level <= priority_level) {

            reset_output_message();

            if (is_timestamp_enable()) {
                append_timestamp();
            }

            if (is_name_enable()) {
                append_name();
            }

            if (is_level_enable()) {
                append_level(msg_level);
            }

            append_msg_separator();
            format_message(args, message);

            log_message();
        }
    }

    [[nodiscard]] bool is_name_enable() const {
        return add_name;
    }

    void reset_output_message() {
        log_output_size = 0;
    }

    void append_name() {
        log_output_append(name);
        log_output_append(separator);
    }

    void append_msg_separator() {
        if (is_name_enable() || is_timestamp_enable() || is_level_enable()) {
            log_output_append(msg_separator);
        }
    }

    void log_message() {
        log_output[log_output_size] = '\0';
        out->append(log_output);
    }

    void format_message(std::va_list args, const char* format) {
        const std::size_t free_space = buffer_info->capacity - log_output_size;
        log_output_size              += vsnprintf(&log_output[log_output_size], free_space, format, args);
    }

    void append_timestamp() {
        util::time::Calendar_t calendar{};

        if (is_time_src_valid()) {

            switch (timestamp_format) {
            case logger::Timestamp_format::hh_mm_ss:
                calendar.time   = time->get_time();
                log_output_size = time::format_time_hh_mm_ss(calendar.time,
                                                             log_output + log_output_size,
                                                             log_output + buffer_info->capacity);
                log_output_append(separator);
                break;

            case logger::Timestamp_format::hh_mm_ss_mmm:
                calendar.time   = time->get_time();
                log_output_size = time::format_time_hh_mm_ss_mmm(calendar.time,
                                                                 log_output + log_output_size,
                                                                 log_output + buffer_info->capacity);
                log_output_append(separator);
                break;

            case logger::Timestamp_format::yyyy_mm_dd:
                calendar.date   = time->get_date();
                log_output_size = time::format_date_yyyy_mm_dd(calendar.date,
                                                               log_output + log_output_size,
                                                               log_output + buffer_info->capacity);
                log_output_append(separator);
                break;

            case logger::Timestamp_format::yyyy_mm_dd_hh_mm_ss:
                calendar        = time->get_calendar();
                log_output_size = time::format_date_time_yyyy_mm_dd_hh_mm_ss(calendar,
                                                                             log_output + log_output_size,
                                                                             log_output + buffer_info->capacity);
                log_output_append(separator);
                break;

            case logger::Timestamp_format::yyyy_mm_dd_hh_mm_ss_mmm:
                calendar        = time->get_calendar();
                log_output_size = time::format_date_time_yyyy_mm_dd_hh_mm_ss_mmm(calendar,
                                                                                 log_output + log_output_size,
                                                                                 log_output + buffer_info->capacity);
                log_output_append(separator);
                break;
            }
        }
    }

    bool is_time_src_valid() {
        return (time != nullptr);
    }

    [[nodiscard]] bool is_timestamp_enable() const {
        return add_timestamp;
    }

    [[nodiscard]] bool is_level_enable() const {
        return add_level;
    }

    void append_level(logger::Level level) {
        auto index = static_cast<std::uint8_t>(level);
        log_output_append(level_str[index]);  // NOLINT(cppcoreguidelines-pro-bounds-constant-array-index)
        log_output_append(separator);
    }

    void log_output_append(const std::string_view& str) {
        for (const auto& it : str) {
            log_output[log_output_size] = it;
            ++log_output_size;

            if (log_output_size == (buffer_info->capacity - str_end)) {
                // Output buffer reached its limit.
                break;
            }
        }
    }

    // Constants
    static constexpr const char*                       separator     = " ";
    static constexpr const char*                       msg_separator = ": ";
    static constexpr std::uint8_t                      str_end       = 1;
    static constexpr std::uint8_t                      nb_level      = 8;
    static constexpr std::array<const char*, nb_level> level_str =
        {"OFF", "FATAL", "ERROR", "WARNING", "INFO", "DEBUG", "TRACE", "ALL"};

    // Output buffer
    logger::Buffer_info_t* buffer_info;
    char*                  log_output;
    std::size_t            log_output_size{0};

    // Variables
    logger::Output*          out;
    std::string_view         name;
    util::time::Time*        time{nullptr};
    logger::Timestamp_format timestamp_format{logger::Timestamp_format::yyyy_mm_dd};

    // Options
    bool          add_level{false};
    bool          add_timestamp{false};
    bool          add_name{false};
    logger::Level priority_level;
};
}  // namespace util::logger

// NOLINTEND(hicpp-no-array-decay)
// NOLINTEND(cppcoreguidelines-pro-bounds-array-to-pointer-decay)
// NOLINTEND(hicpp-vararg)
// NOLINTEND(cppcoreguidelines-pro-type-vararg)
// NOLINTEND(cert-dcl50-cpp)
#endif /* LOGGER_H */
