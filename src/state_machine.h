/**
 * Copyright 2023 Christian Daigneault-St-Arnaud
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
 * following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions
 *    and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the
 *    following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote
 *    products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef STATE_MACHINE_H
#define STATE_MACHINE_H

#include <cstdint>
#include <optional>

namespace util::sm {

using state_name_t = uint32_t;
using event_name_t = uint32_t;

/*  State machine state.
 *  Parameter(s) MUST be handled by the implementation. The execute method can return an event
 *  and the state machine will handle it right away.
 */
class State {
public:
    explicit State(state_name_t state_name)
    : name(state_name){};

    // Return an event_name_t if the state execution generate a new event.
    virtual std::optional<event_name_t> execute() = 0;

    virtual ~State() = default;

    [[nodiscard]] state_name_t get_name() const {
        return name;
    }

    State()                        = default;  // Default constructor
    State(const State&)            = delete;   // Non construction-copyable
    State& operator=(const State&) = delete;   // non copyable
    State(State&&)                 = delete;   // non movable
    State& operator=(State&&)      = delete;   // No move assignment operator

private:
    state_name_t name{};
};

/*  State machine.
 *  Execute the state associated to an event following the given state transition matrix.
 */
class State_machine {
public:
    struct State_transition {
        State*       current_state;
        event_name_t event;
        State*       next_state;
    };

    State_machine() = default;

    void init(State& init_state, State_transition* begin, State_transition* end) {
        current_state = &init_state;
        matrix_begin  = begin;
        matrix_end    = end;
    }

    bool process(event_name_t event) {
        std::optional<event_name_t> internal_event{event};

        do {
            State* next_state = get_next_state(internal_event.value());

            if (is_new_state_valid(next_state)) {
                current_state  = next_state;
                internal_event = current_state->execute();
            } else {
                return false;
            }
        } while (internal_event);

        return true;
    }

    void handle_error(State* error_state) {
        current_state = error_state;
        error_state->execute();
    }

    State* get_current_state() {
        return current_state;
    }

private:
    State* get_next_state(event_name_t event) {
        State_transition* state_it = find(current_state->get_name(), event);

        if (is_transition_found(state_it)) {
            return (state_it->next_state);
        }
        return nullptr;
    }

    State_transition* find(state_name_t state_name, event_name_t event) {

        for (auto* i = matrix_begin; i != matrix_end; ++i) {
            if (i->current_state == nullptr) {
                return matrix_end;
            }

            if ((i->current_state->get_name() == state_name) && (i->event == event)) {
                return i;
            }
        }
        return matrix_end;
    }

    bool is_transition_found(State_transition* state_it) {
        return state_it != matrix_end;
    }

    static bool is_new_state_valid(State* state) {
        return state != nullptr;
    }

    State*            current_state;
    State_transition* matrix_begin;
    State_transition* matrix_end;
};
}  // namespace util::sm

#endif /* STATE_MACHINE_H */
