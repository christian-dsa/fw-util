/**
 * Copyright 2023 Christian Daigneault-St-Arnaud
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
 * following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions
 *    and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the
 *    following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote
 *    products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef LITTLEFS_H
#define LITTLEFS_H

#include "lfs.h"
#include <cstdint>
#include <type_traits>

namespace util::littlefs {

// NOLINTBEGIN(cppcoreguidelines-pro-type-reinterpret-cast)

/// Block device
class Block_device {
public:
    // Read a region in a block. Negative error codes are propagated
    // to the user.
    virtual int
    read(const struct lfs_config* config, lfs_block_t block, lfs_off_t off, void* buffer, lfs_size_t size) = 0;

    // Program a region in a block. The block must have previously
    // been erased. Negative error codes are propagated to the user.
    // May return LFS_ERR_CORRUPT if the block should be considered bad.
    virtual int
    prog(const struct lfs_config* config, lfs_block_t block, lfs_off_t off, const void* buffer, lfs_size_t size) = 0;

    // Erase a block. A block must be erased before being programmed.
    // The state of an erased block is undefined. Negative error codes
    // are propagated to the user.
    // May return LFS_ERR_CORRUPT if the block should be considered bad.
    virtual int erase(const struct lfs_config* config, lfs_block_t block) = 0;

    // Sync the state of the underlying block device. Negative error codes
    // are propagated to the user.
    virtual int sync(const struct lfs_config* config) = 0;

#ifdef LFS_THREADSAFE
    // Lock the underlying block device. Negative error codes
    // are propagated to the user.
    virtual int lock(const struct lfs_config* c) = 0;

    // Unlock the underlying block device. Negative error codes
    // are propagated to the user.
    virtual int unlock(const struct lfs_config* c) = 0;
#endif

    virtual ~Block_device() = default;

    Block_device()                               = default;  // Default constructor
    Block_device(const Block_device&)            = delete;   // Non construction-copyable
    Block_device& operator=(const Block_device&) = delete;   // non copyable
    Block_device(Block_device&&)                 = delete;   // non movable
    Block_device& operator=(Block_device&&)      = delete;   // No move assignment operator
};

/// Types
using info = struct lfs_info;

enum class Error {
    ok            = 0,    // No error
    io            = -5,   // Error during device operation
    corrupt       = -84,  // Corrupted
    no_entry      = -2,   // No directory entry
    exist         = -17,  // Entry already exists
    not_dir       = -20,  // Entry is not a dir
    is_dir        = -21,  // Entry is a dir
    not_empty     = -39,  // Dir is not empty
    bad_file      = -9,   // Bad file number
    too_big       = -27,  // File too large
    invalid       = -22,  // Invalid parameter
    no_space      = -28,  // No space left on device
    no_memory     = -12,  // No more memory available
    no_attribute  = -61,  // No data/attr available
    name_too_long = -36,  // File name too long
};

static Error unused_error{};  // NOLINT

static bool is_error(int error_code) {
    return (error_code < 0);
}

/// File system
struct Fs_config {
    lfs_size_t   read_size        = 0;
    lfs_size_t   prog_size        = 0;
    lfs_size_t   block_size       = 0;
    lfs_size_t   block_count      = 0;
    std::int32_t block_cycles     = 0;
    lfs_size_t   name_max         = 0;
    lfs_size_t   file_max         = 0;
    lfs_size_t   attr_max         = 0;
    lfs_size_t   metadata_max     = 0;
    void*        read_buffer      = nullptr;
    void*        prog_buffer      = nullptr;
    lfs_size_t   cache_size       = 0;
    void*        lookahead_buffer = nullptr;
    lfs_size_t   lookahead_size   = 0;
};

class File_system {
public:
    File_system() = default;

    void init(Block_device& bd, Fs_config& fs_config, Error& error = unused_error) {
        error          = Error::ok;
        cache_size     = fs_config.cache_size;
        config.context = &bd;

        config.read =
            [](const struct lfs_config* cfg, lfs_block_t block, lfs_off_t off, void* buffer, lfs_size_t size) {
                return reinterpret_cast<Block_device*>(cfg->context)->read(cfg, block, off, buffer, size);
            };

        config.prog =
            [](const struct lfs_config* cfg, lfs_block_t block, lfs_off_t off, const void* buffer, lfs_size_t size) {
                return reinterpret_cast<Block_device*>(cfg->context)->prog(cfg, block, off, buffer, size);
            };

        config.erase = [](const struct lfs_config* cfg, lfs_block_t block) {
            return reinterpret_cast<Block_device*>(cfg->context)->erase(cfg, block);
        };

        config.sync = [](const struct lfs_config* cfg) {
            return reinterpret_cast<Block_device*>(cfg->context)->sync(cfg);
        };

#ifdef LFS_THREADSAFE
        config.lock = = [](const struct lfs_config* c) {
            return reinterpret_cast<Littlefs_io*>(c->context)->lock(c);
        };

        config.unlock = = [](const struct lfs_config* c) {
            return reinterpret_cast<Littlefs_io*>(c->context)->unlock(c);
        };
#endif
        config.read_size        = fs_config.read_size;
        config.prog_size        = fs_config.prog_size;
        config.block_size       = fs_config.block_size;
        config.block_count      = fs_config.block_count;
        config.block_cycles     = fs_config.block_cycles;
        config.cache_size       = fs_config.cache_size;
        config.lookahead_size   = fs_config.lookahead_size;
        config.read_buffer      = fs_config.read_buffer;
        config.prog_buffer      = fs_config.prog_buffer;
        config.lookahead_buffer = fs_config.lookahead_buffer;
        config.name_max         = fs_config.name_max;
        config.file_max         = fs_config.file_max;
        config.attr_max         = fs_config.attr_max;
        config.metadata_max     = fs_config.metadata_max;

        // block_size Must be a multiple of the read and program sizes.
        if (((config.block_size % config.read_size) > 0) || ((config.block_size % config.prog_size) > 0)) {
            error = Error::invalid;
        }
        // cache_size Must be a multiple of the read and program sizes, and a factor of the block size.
        if (((config.cache_size % config.read_size) > 0) || ((config.cache_size % config.prog_size) > 0) ||
            ((config.block_size % config.cache_size) > 0)) {
            error = Error::invalid;
        }

        // lookahead_size Must be a multiple of 8.
        // lookahead_buffer Must be aligned to a 32-bit boundary.
        static constexpr std::size_t needed_multiple  = 8;
        static constexpr std::size_t needed_alignment = 4;
        if (((config.lookahead_size % needed_multiple) > 0) ||
            ((reinterpret_cast<uintptr_t>(config.lookahead_buffer) % needed_alignment) > 0)) {
            error = Error::invalid;
        }

        if ((config.read_buffer == nullptr) || (config.prog_buffer == nullptr) ||
            (config.lookahead_buffer == nullptr)) {
            error = Error::no_memory;
        }
    }

    lfs_t& get_lfs() {
        return lfs;
    }

    /// Filesystem operation
#ifndef LFS_READONLY
    void format(Error& error = unused_error) {
        error = static_cast<Error>(lfs_format(&lfs, &config));
    }
#endif

    void mount(Error& error = unused_error) {
        const int temp_error = lfs_mount(&lfs, &config);
        error                = static_cast<Error>(temp_error);

        is_fs_mounted = !is_error(temp_error);
    }

    void unmount(Error& error = unused_error) {
        error         = static_cast<Error>(lfs_unmount(&lfs));
        is_fs_mounted = false;
    }

    [[nodiscard]] bool is_mount() const {
        return is_fs_mounted;
    }

#ifndef LFS_READONLY
    void remove(const char* path, Error& error = unused_error) {
        error = static_cast<Error>(lfs_remove(&lfs, path));
    }
#endif

#ifndef LFS_READONLY
    void rename(const char* old_path, const char* new_path, Error& error = unused_error) {
        error = static_cast<Error>(lfs_rename(&lfs, old_path, new_path));
    }
#endif

    void statistics(const char* path, info& file_info, Error& error = unused_error) {
        error = static_cast<Error>(lfs_stat(&lfs, path, &file_info));
    }

    lfs_ssize_t
    get_attribute(const char* path, uint8_t type, void* buffer, lfs_size_t size, Error& error = unused_error) {
        const lfs_ssize_t output = lfs_getattr(&lfs, path, type, buffer, size);

        // Error
        if (is_error(output)) {
            error = static_cast<Error>(output);
            return 0;
        }

        error = Error::ok;
        return output;
    }

    void
    set_attribute(const char* path, uint8_t type, const void* buffer, lfs_size_t size, Error& error = unused_error) {
        error = static_cast<Error>(lfs_setattr(&lfs, path, type, buffer, size));
    }

    void remove_attribute(const char* path, uint8_t type, Error& error = unused_error) {
        error = static_cast<Error>(lfs_removeattr(&lfs, path, type));
    }

    [[nodiscard]] lfs_size_t get_cache_size() const {
        return cache_size;
    }

    /// Directory operation
#ifndef LFS_READONLY
    void mkdir(const char* path, Error& error = unused_error) {
        error = static_cast<Error>(lfs_mkdir(&lfs, path));
    }
#endif
    void open(const char* path, Error& error = unused_error) {
        if (is_directory_open()) {
            error = Error::invalid;
            return;
        }
        error = static_cast<Error>(lfs_dir_open(&lfs, &dir, path));
    }

    void close(Error& error = unused_error) {
        if (is_directory_open()) {
            error = static_cast<Error>(lfs_dir_close(&lfs, &dir));
        } else {
            error = Error::invalid;
        }
    }

    int read(info& dir_info, Error& error = unused_error) {
        error            = Error::ok;
        const int output = lfs_dir_read(&lfs, &dir, &dir_info);

        if (is_error(output)) {
            error = static_cast<Error>(output);
        }

        // 0: End of directory
        // 1: Entry is found
        return output;
    }

    void seek(lfs_off_t off, Error& error = unused_error) {
        error            = Error::ok;
        const int output = lfs_dir_seek(&lfs, &dir, off);

        if (is_error(output)) {
            error = static_cast<Error>(output);
        }
    }

    lfs_soff_t tell(Error& error = unused_error) {
        error             = Error::ok;
        lfs_soff_t output = lfs_dir_tell(&lfs, &dir);

        if (is_error(output)) {
            error  = static_cast<Error>(output);
            output = 0;
        }
        return output;
    }

    void rewind(Error& error = unused_error) {
        error            = Error::ok;
        const int output = lfs_dir_rewind(&lfs, &dir);

        if (is_error(output)) {
            error = static_cast<Error>(output);
        }
    }

    // TODO Add Filesystem-level filesystem operations
private:
    bool is_directory_open() {
        struct lfs::lfs_mlist* head = lfs.mlist;
        auto*                  node = reinterpret_cast<struct lfs::lfs_mlist*>(&dir);

        for (struct lfs::lfs_mlist** p = &head; *p != nullptr; p = &(*p)->next) {
            if (*p == node) {
                return true;
            }
        }
        return false;
    }

    lfs_t      lfs{};
    lfs_config config{};
    lfs_dir_t  dir{};
    lfs_size_t cache_size{};
    bool       is_fs_mounted{false};
};

/// File
class File {
public:
    File(File_system& fs, void* cache)
    : lfs(&fs.get_lfs()),
      file(),
      file_config({.buffer = cache, .attrs = nullptr, .attr_count = 0}){};

    // Keep LFS flags as-is
    // NOLINTBEGIN(readability-identifier-naming)
    enum Open_flags : std::uint32_t {
        // open flags
        LFS_O_RDONLY = 1U,  // Open a file as read only
#ifndef LFS_READONLY
        LFS_O_WRONLY = 2U,       // Open a file as write only
        LFS_O_RDWR   = 3U,       // Open a file as read and write
        LFS_O_CREAT  = 0x0100U,  // Create a file if it does not exist
        LFS_O_EXCL   = 0x0200U,  // Fail if a file already exists
        LFS_O_TRUNC  = 0x0400U,  // Truncate the existing file to zero size
        LFS_O_APPEND = 0x0800U,  // Move to end of file on every write
#endif
    };
    // NOLINTEND(readability-identifier-naming)

    enum class Seek {
        set     = 0,  // Seek relative to an absolute position
        current = 1,  // Seek relative to the current file position
        end     = 2,  // Seek relative to the end of the file
    };

    void open(const char* path, const char* mode, Error& error = unused_error) {
        if (is_file_open()) {
            error = Error::invalid;
            return;
        }

        if (file_config.buffer == nullptr) {
            error = Error::no_memory;
            return;
        }

        const int open_flags = mode_to_lfs_flags(mode);
        error                = static_cast<Error>(lfs_file_opencfg(lfs, &file, path, open_flags, &file_config));
    }

    void close(Error& error = unused_error) {
        if (!is_file_open()) {
            error = Error::invalid;
            return;
        }
        error = static_cast<Error>(lfs_file_close(lfs, &file));
    }

    void sync(Error& error = unused_error) {
        if (!is_file_open()) {
            error = Error::invalid;
            return;
        }
        error = static_cast<Error>(lfs_file_sync(lfs, &file));
    }

    template<typename type>
    lfs_ssize_t read(type& object, Error& error = unused_error) {
        if (!is_read_allowed()) {
            error = Error::invalid;
            return 0;
        }

        lfs_ssize_t output = lfs_file_read(lfs, &file, reinterpret_cast<void*>(&object), sizeof(object));

        // Error
        if (is_error(output)) {
            error = static_cast<Error>(output);
            return 0;
        }

        error = Error::ok;
        return output;
    }

    template<typename iterator>
    lfs_ssize_t read(iterator begin, lfs_size_t size, Error& error = unused_error) {
        if (!is_read_allowed()) {
            error = Error::invalid;
            return 0;
        }

        lfs_ssize_t output = lfs_file_read(lfs, &file, reinterpret_cast<void*>(begin), size);

        // Error
        if (is_error(output)) {
            error = static_cast<Error>(output);
            return 0;
        }

        error = Error::ok;
        return output;
    }

#ifndef LFS_READONLY
    template<typename type>
    lfs_ssize_t write(const type& object, Error& error = unused_error) {
        if (!is_write_allowed()) {
            error = Error::invalid;
            return 0;
        }

        lfs_ssize_t output = lfs_file_write(lfs, &file, reinterpret_cast<const void*>(&object), sizeof(object));

        // Error
        if (is_error(output)) {
            error = static_cast<Error>(output);
            return 0;
        }

        error = Error::ok;
        return output;
    }

    template<typename iterator>
    lfs_ssize_t write(const iterator& begin, lfs_size_t size, Error& error = unused_error) {
        if (!is_write_allowed()) {
            error = Error::invalid;
            return 0;
        }

        lfs_ssize_t output = lfs_file_write(lfs, &file, reinterpret_cast<const void*>(begin), size);

        // Error
        if (is_error(output)) {
            error = static_cast<Error>(output);
            return 0;
        }

        error = Error::ok;
        return static_cast<std::int32_t>(output);
    }
#endif

    lfs_soff_t seek(lfs_soff_t off, Seek flags, Error& error = unused_error) {
        if (!is_file_open()) {
            error = Error::invalid;
            return 0;
        }

        const lfs_soff_t output = lfs_file_seek(lfs, &file, off, static_cast<int>(flags));

        // Error
        if (is_error(output)) {
            error = static_cast<Error>(output);
            return 0;
        }

        error = Error::ok;
        return output;
    }

#ifndef LFS_READONLY
    void truncate(lfs_off_t size, Error& error = unused_error) {
        if (!is_file_open()) {
            error = Error::invalid;
            return;
        }
        error = static_cast<Error>(lfs_file_truncate(lfs, &file, size));
    }
#endif

    lfs_soff_t tell(Error& error = unused_error) {
        if (!is_file_open()) {
            error = Error::invalid;
            return 0;
        }

        const lfs_soff_t output = lfs_file_tell(lfs, &file);

        // Error
        if (is_error(output)) {
            error = static_cast<Error>(output);
            return 0;
        }

        error = Error::ok;
        return output;
    }

    void rewind(Error& error = unused_error) {
        if (!is_file_open()) {
            error = Error::invalid;
            return;
        }
        error = static_cast<Error>(lfs_file_rewind(lfs, &file));
    }

    lfs_soff_t size(Error& error = unused_error) {
        if (!is_file_open()) {
            error = Error::invalid;
            return -1;
        }

        const lfs_soff_t output = lfs_file_size(lfs, &file);

        // Error
        if (is_error(output)) {
            error = static_cast<Error>(output);
            return 0;
        }

        error = Error::ok;
        return output;
    }

    lfs_size_t get_cache_size() {
        return lfs->cfg->cache_size;
    }

private:
    static int mode_to_lfs_flags(const char* mode) {
        std::uint16_t flags = 0;

        switch (mode[0]) {
        case 'r':
            flags = LFS_O_RDONLY;
            if (is_mode_plus(mode)) {
                flags |= LFS_O_WRONLY;
            }
            break;
        case 'w':
            flags = LFS_O_CREAT | LFS_O_WRONLY | LFS_O_TRUNC;
            if (is_mode_plus(mode)) {
                flags |= LFS_O_RDONLY;
            }
            break;
        case 'a':
            flags = LFS_O_CREAT | LFS_O_WRONLY | LFS_O_APPEND;
            if (is_mode_plus(mode)) {
                flags |= LFS_O_RDONLY;
            }
            break;
        default:
            break;
        }

        return flags;
    }

    bool is_file_open() {
        struct lfs::lfs_mlist* head = lfs->mlist;
        auto*                  node = reinterpret_cast<struct lfs::lfs_mlist*>(&file);

        for (struct lfs::lfs_mlist** p = &head; *p != nullptr; p = &(*p)->next) {
            if (*p == node) {
                return true;
            }
        }
        return false;
    }

    bool is_read_allowed() {
        return ((file.flags & LFS_O_RDONLY) == LFS_O_RDONLY) && is_file_open();
    }

    bool is_write_allowed() {
        return ((file.flags & LFS_O_WRONLY) == LFS_O_WRONLY) && is_file_open();
    }

    static bool is_mode_plus(const char* mode) {
        return mode[1] == '+';
    }

    // Variables
    lfs_t*          lfs;
    lfs_file_t      file;
    lfs_file_config file_config;

    // File mode

    // Opens a file for reading. The file must exist.
    static constexpr const char* read_only = "r";

    // Creates an empty file for writing. If a file with the same name already exists, its content is erased
    // and the file is considered as a new empty file.
    static constexpr const char* write_only = "w";

    // Appends to a file. Writing operations, append data at the end of the file. The file is created if it does not
    // exist.
    static constexpr const char* append_only = "a";

    // Opens a file to update both reading and writing. The file must exist.
    static constexpr const char* read_write = "r+";

    // Creates an empty file for both reading and writing.
    static constexpr const char* write_read = "w+";

    // Opens a file for reading and appending. The file is created if it does not exist.
    static constexpr const char* append_read = "a+";
};
}  // namespace util::littlefs

// NOLINTEND(cppcoreguidelines-pro-type-reinterpret-cast)
#endif  // LITTLEFS_H
