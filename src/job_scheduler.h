/**
 * Copyright 2023 Christian Daigneault-St-Arnaud
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
 * following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions
 *    and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the
 *    following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote
 *    products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef JOB_SCHEDULER_H
#define JOB_SCHEDULER_H

#include "time_util.h"

namespace util::job_scheduler {

class Job_schedule {
public:
    // types
    using timestamp_t = std::uint32_t;
    using weekday_t   = std::uint8_t;

    struct Job {
        void (*execute)(void* args);
        void* arguments;
    };

    using schedule_entry_t = std::pair<timestamp_t, Job>;

    Job_schedule(schedule_entry_t* begin, schedule_entry_t* end)
    : it_begin(begin),
      it_end(end) {}

    using iterator = schedule_entry_t*;

    [[nodiscard]] iterator begin() const {
        return it_begin;
    }

    [[nodiscard]] iterator end() const {
        return it_begin + schedule_size;
    }

    [[nodiscard]] std::size_t size() const {
        return schedule_size;
    }

    void clear() {
        schedule_size = 0;
    }

    [[nodiscard]] bool empty() const {
        return schedule_size == 0;
    }

    void insert(iterator pos, const schedule_entry_t& value) {
        std::copy_backward(pos, end(), end() + 1);
        *pos = value;
        ++schedule_size;
    }

    void erase(iterator pos) {
        std::copy(pos + 1, end(), pos);
        --schedule_size;
    }

    [[nodiscard]] std::size_t capacity() const {
        return it_end - it_begin;
    }

    schedule_entry_t& operator[](std::size_t index) const {
        return *(it_begin + index);
    }

private:
    iterator    it_begin;
    iterator    it_end;
    std::size_t schedule_size{0};
};

class Job_scheduler {
public:
    // types
    using timestamp_t = std::uint32_t;
    using weekday_t   = std::uint8_t;

    // methods
    explicit Job_scheduler(Job_schedule& schedule)
    : current_schedule(&schedule),
      search_it(schedule.begin()),
      late_time({0, 0, 0, 0}) {}

    bool insert(timestamp_t timestamp, Job_schedule::Job job) {
        bool  found_position = false;
        auto* it             = current_schedule->begin();
        bool  success        = true;

        if (current_schedule->size() < current_schedule->capacity()) {
            do {
                if ((it == current_schedule->end()) || (timestamp < it->first)) {
                    found_position = true;
                    current_schedule->insert(it, {timestamp, job});
                } else if (timestamp == it->first) {
                    // Do not overwrite entry with existing timestamp
                    found_position = true;
                    success        = false;
                } else {
                    ++it;
                }
            } while (!found_position);
        } else {
            success = false;
        }

        return success;
    }

    bool insert(util::time::Time_t time, util::time::Date_t date, Job_schedule::Job job) {
        timestamp_t       timestamp = time_to_timestamp(time);
        const timestamp_t month     = (date.month << month_pos);
        const timestamp_t day       = (date.day << day_pos);
        timestamp                   |= month_day_format | month | day;

        return insert(timestamp, job);
    }

    bool insert(util::time::Time_t time, weekday_t week_day, Job_schedule::Job job) {
        timestamp_t timestamp = time_to_timestamp(time);
        timestamp             |= weekday_format | week_day;

        return insert(timestamp, job);
    }

    void erase(timestamp_t timestamp) {
        auto* it = find(timestamp);

        if (it != current_schedule->end()) {
            current_schedule->erase(it);
        }
    }

    void execute(util::time::Calendar_t current_time) {

        time::Time_t acceptable_time = current_time.time - late_time;

        const timestamp_t current_time_fmt    = time_to_timestamp(current_time.time);
        const timestamp_t acceptable_time_fmt = time_to_timestamp(acceptable_time);

        if (is_search_reset_needed(current_time_fmt)) {
            reset_search();
        }

        while ((search_it != current_schedule->end()) && (!is_job_time_greater_than_time(current_time_fmt))) {

            if ((get_job_time() >= acceptable_time_fmt) && (get_job_time() <= current_time_fmt)) {

                if ((is_week_day_format() && is_same_week_day(current_time)) || is_same_date(current_time)) {
                    execute_job();
                }
            }
            ++search_it;
        }
    }

    void allow_to_be_late_by(util::time::Time_t time) {
        late_time = time;
    }

    const Job_schedule& get_schedule() {
        return *current_schedule;
    }

    void set_schedule(Job_schedule& schedule) {
        current_schedule = &schedule;
        reset_search();
    }

    static util::time::Time_t timestamp_to_time(const timestamp_t timestamp) {
        static constexpr std::uint8_t hour_mask      = 0x1F;  // Max value (23) use 5 bit.
        static constexpr std::uint8_t min_mask       = 0x3F;  // Max value (59) use 6 bit.
        static constexpr std::uint8_t sec_mask       = 0x3F;  // Max value (59) use 6 bit.
        const timestamp_t             timestamp_time = timestamp & time_mask;

        return {.hour = static_cast<int32_t>((timestamp_time >> hour_pos) & hour_mask),
                .min  = static_cast<uint8_t>((timestamp_time >> min_pos) & min_mask),
                .sec  = static_cast<uint8_t>((timestamp_time >> sec_pos) & sec_mask),
                .ms   = 0};
    }

private:
    Job_schedule::iterator find(timestamp_t timestamp) {
        for (auto* it = current_schedule->begin(); it != current_schedule->end(); ++it) {
            if (timestamp == it->first) {
                return it;
            }
        }
        return current_schedule->end();
    }

    static timestamp_t time_to_timestamp(util::time::Time_t& time) {
        const timestamp_t hour = static_cast<timestamp_t>(time.hour) << hour_pos;
        const timestamp_t min  = (time.min << min_pos);
        const timestamp_t sec  = (time.sec << sec_pos);
        return hour | min | sec;
    }

    bool is_job_time_greater_than_time(timestamp_t current_time) {
        return ((search_it->first & time_mask) > current_time);
    }

    timestamp_t get_job_time() {
        return search_it->first & time_mask;
    }

    void execute_job() {
        search_it->second.execute(search_it->second.arguments);
    }

    bool is_search_reset_needed(timestamp_t current_time) {
        return (search_it == current_schedule->end()) &&
               (current_time <= (current_schedule->begin()->first & time_mask));
    }

    void reset_search() {
        search_it = current_schedule->begin();
    }

    bool is_week_day_format() {
        return (search_it->first & option_mask) == weekday_format;
    }

    bool is_same_week_day(util::time::Calendar_t& current_time) {
        bool is_same = false;

        switch (current_time.date.week_day) {
        case util::time::Week_day::sunday:
            if ((search_it->first & sunday) > 0) {
                is_same = true;
            }
            break;
        case util::time::Week_day::monday:
            if ((search_it->first & monday) > 0) {
                is_same = true;
            }
            break;
        case util::time::Week_day::tuesday:
            if ((search_it->first & tuesday) > 0) {
                is_same = true;
            }
            break;
        case util::time::Week_day::wednesday:
            if ((search_it->first & wednesday) > 0) {
                is_same = true;
            }
            break;
        case util::time::Week_day::thursday:
            if ((search_it->first & thursday) > 0) {
                is_same = true;
            }
            break;
        case util::time::Week_day::friday:
            if ((search_it->first & friday) > 0) {
                is_same = true;
            }
            break;
        case util::time::Week_day::saturday:
            if ((search_it->first & saturday) > 0) {
                is_same = true;
            }
            break;
        default:
            break;
        }
        return is_same;
    }

    bool is_same_date(util::time::Calendar_t& current_time) {
        const std::uint8_t month = (search_it->first & month_mask) >> month_pos;
        const std::uint8_t day   = (search_it->first & day_mask) >> day_pos;

        return (month == current_time.date.month) && (day == current_time.date.day);
    }

    // variables
    Job_schedule*          current_schedule;
    Job_schedule::iterator search_it;
    util::time::Time_t     late_time;

    // constants

    // timestamp_t bitfield
    static constexpr std::uint8_t hour_pos    = 27;  // Bits 31 ... 27
    static constexpr std::uint8_t min_pos     = 21;  // Bits 26 ... 21
    static constexpr std::uint8_t sec_pos     = 15;  // Bits 20 ... 15
    static constexpr std::uint8_t opt_pos     = 14;  // Bits 14 ... 14
    static constexpr std::uint8_t month_pos   = 5;   // Bits  8 ...  5
    static constexpr std::uint8_t day_pos     = 0;   // Bits  4 ...  0
    static constexpr std::uint8_t weekday_pos = 0;   // Bits  6 ...  0

    // weekday bitfield
    static constexpr std::uint8_t sunday_pos    = 6;
    static constexpr std::uint8_t monday_pos    = 5;
    static constexpr std::uint8_t tuesday_pos   = 4;
    static constexpr std::uint8_t wednesday_pos = 3;
    static constexpr std::uint8_t thursday_pos  = 2;
    static constexpr std::uint8_t friday_pos    = 1;
    static constexpr std::uint8_t saturday_pos  = 0;

    // time mask
    static constexpr timestamp_t time_mask = 0xFFFF8000;

    // date mask
    static constexpr timestamp_t month_mask = 0x000001e0;
    static constexpr timestamp_t day_mask   = 0x0000001F;

    // option mask
    static constexpr timestamp_t option_mask = 1U << opt_pos;

    // options
    static constexpr timestamp_t month_day_format = 0U << opt_pos;
    static constexpr timestamp_t weekday_format   = 1U << opt_pos;

public:
    // weekday
    static constexpr weekday_t sunday    = 1U << sunday_pos;
    static constexpr weekday_t monday    = 1U << monday_pos;
    static constexpr weekday_t tuesday   = 1U << tuesday_pos;
    static constexpr weekday_t wednesday = 1U << wednesday_pos;
    static constexpr weekday_t thursday  = 1U << thursday_pos;
    static constexpr weekday_t friday    = 1U << friday_pos;
    static constexpr weekday_t saturday  = 1U << saturday_pos;
    static constexpr weekday_t weekday   = monday | tuesday | wednesday | thursday | friday;  // NOLINT
    static constexpr weekday_t weekend   = sunday | saturday;
    static constexpr weekday_t everyday  = weekday | weekend;
};
}  // namespace util::job_scheduler

#endif  // JOB_SCHEDULER_H
