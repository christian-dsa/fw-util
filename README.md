# FW-Util

**Develop**  
[![pipeline status](https://gitlab.com/christian-dsa/fw-util/badges/develop/pipeline.svg)](https://gitlab.com/christian-dsa/fw-util/-/commits/develop)  
[![coverage report](https://gitlab.com/christian-dsa/fw-util/badges/develop/coverage.svg)](https://gitlab.com/christian-dsa/fw-util/-/commits/develop)

# Littlefs subtree
git-subtree-dir: vendor/littlefs
git-subtree-split: 40dba4a556e0d81dfbe64301a6aa4e18ceca896c
